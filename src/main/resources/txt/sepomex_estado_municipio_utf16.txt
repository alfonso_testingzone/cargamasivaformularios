1	1	0	Aguascalientes	
1	1	1	Aguascalientes	Aguascalientes
1	2	0	Asientos	
1	2	7	Asientos	Asientos
1	3	0	Calvillo	
1	3	2	Calvillo	Calvillo
1	4	0	Cosío	
1	4	9	Cosío	Cosío
1	5	0	Jesús María	
1	5	4	Jesús María	Jesús María
1	6	0	Pabellón de Arteaga	
1	6	5	Pabellón de Arteaga	Pabellón de Arteaga
1	7	0	Rincón de Romos	
1	7	6	Rincón de Romos	Rincón de Romos
1	8	0	San José de Gracia	
1	9	0	Tepezalá	
1	9	8	Tepezalá	Tepezalá
1	10	0	El Llano	
1	11	0	San Francisco de los Romo	
1	11	3	San Francisco de los Romo	San Francisco de los Romo
2	1	1	Ensenada	Ensenada
2	1	6	Ensenada	Rodolfo Sánchez T. (Maneadero)
2	2	2	Mexicali	Mexicali
2	2	7	Mexicali	San Felipe
2	3	3	Tecate	Tecate
2	4	4	Tijuana	Tijuana
2	5	5	Playas de Rosarito	Playas de Rosarito
3	1	1	Comondú	Ciudad Constitución
3	1	6	Comondú	Puerto Adolfo López Mateos
3	2	8	Mulegé	Heroica Mulegé
3	2	9	Mulegé	Villa Alberto Andrés Alvarado Arámburo
3	2	10	Mulegé	San Ignacio
3	2	11	Mulegé	Guerrero Negro
3	2	12	Mulegé	Santa Rosalía
3	3	2	La Paz	La Paz
3	3	7	La Paz	Todos Santos
3	8	3	Los Cabos	Cabo San Lucas
3	8	4	Los Cabos	San José del Cabo
3	9	5	Loreto	Loreto
4	1	3	Calkiní	Calkini
4	2	1	Campeche	San Francisco de Campeche
4	3	2	Carmen	Ciudad del Carmen
4	3	6	Carmen	Sabancuy
4	4	8	Champotón	Champotón
4	5	9	Hecelchakán	Hecelchakán
4	5	11	Hecelchakán	Pomuch
4	6	7	Hopelchén	Hopelchén
4	9	5	Escárcega	Escárcega
4	11	4	Candelaria	Candelaria
5	2	1	Acuña	Ciudad Acuña
5	3	24	Allende	Allende
5	4	23	Arteaga	Arteaga
5	6	12	Castaños	Castaños
5	7	14	Cuatro Ciénegas	Cuatro Ciénegas de Carranza
5	9	13	Francisco I. Madero	Francisco I. Madero (Chávez)
5	10	2	Frontera	Frontera
5	17	3	Matamoros	Matamoros
5	18	4	Monclova	Monclova
5	19	22	Morelos	Morelos
5	20	20	Múzquiz	Ciudad Melchor Múzquiz
5	21	15	Nadadores	Nadadores
5	22	17	Nava	Nava
5	24	5	Parras	Parras de la Fuente
5	25	6	Piedras Negras	Piedras Negras
5	27	16	Ramos Arizpe	Ramos Arizpe
5	28	7	Sabinas	Sabinas
5	30	8	Saltillo	Saltillo
5	31	19	San Buenaventura	San Buenaventura
5	32	9	San Juan de Sabinas	Nueva Rosita
5	33	10	San Pedro	San Pedro
5	35	11	Torreón	Torreón
5	36	21	Viesca	Viesca
5	38	18	Zaragoza	Zaragoza
6	1	5	Armería	Ciudad de Armería
6	7	2	Manzanillo	Manzanillo
6	9	3	Tecomán	Tecoman
6	10	4	Villa de Álvarez	Ciudad de Villa de Álvarez
7	2	25	Acala	Acala
7	9	9	Arriaga	Arriaga
7	15	16	Cacahoatán	Cacahoatán
7	17	18	Cintalapa	Cintalapa de Figueroa
7	19	1	Comitán de Domínguez	Comitán de Domínguez
7	27	15	Chiapa de Corzo	Chiapa de Corzo
7	40	23	Huixtla	Huixtla
7	46	6	Jiquipilas	Jiquipilas
7	51	13	Mapastepec	Mapastepec
7	52	8	Las Margaritas	Las Margaritas
7	57	24	Motozintla	Motozintla de Mendoza
7	59	11	Ocosingo	Ocosingo
7	61	17	Ocozocoautla de Espinosa	Ocozocoautla de Espinosa
7	65	10	Palenque	Palenque
7	68	19	Pichucalco	Pichucalco
7	69	21	Pijijiapan	Pijijiapan
7	74	22	Reforma	Reforma
7	75	14	Las Rosas	Las Rosas
7	78	2	San Cristóbal de las Casas	San Cristóbal de las Casas
7	89	3	Tapachula	Tapachula de Córdova y Ordóñez
7	89	20	Tapachula	Puerto Madero (San Benito)
7	97	12	Tonalá	Tonalá
7	101	4	Tuxtla Gutiérrez	Tuxtla Gutiérrez
7	106	5	Venustiano Carranza	Venustiano Carranza
7	108	7	Villaflores	Villaflores
8	2	11	Aldama	Juan Aldama
8	6	14	Bachíniva	Bachíniva
8	11	1	Camargo	Santa Rosalía de Camargo
8	17	3	Cuauhtémoc	Cuauhtémoc
8	17	10	Cuauhtémoc	Colonia Anáhuac
8	19	2	Chihuahua	Chihuahua
8	21	4	Delicias	Delicias
8	32	5	Hidalgo del Parral	Hidalgo del Parral
8	36	12	Jiménez	José Mariano Jiménez
8	37	6	Juárez	Juárez
8	40	9	Madera	Madera
8	50	7	Nuevo Casas Grandes	Nuevo Casas Grandes
8	52	13	Ojinaga	Manuel Ojinaga
8	62	15	Saucillo	Saucillo
9	2	2	Azcapotzalco	Ciudad de México
9	3	4	Coyoacán	Ciudad de México
9	4	5	Cuajimalpa de Morelos	Ciudad de México
9	5	7	Gustavo A. Madero	Ciudad de México
9	6	8	Iztacalco	Ciudad de México
9	7	9	Iztapalapa	Ciudad de México
9	8	10	La Magdalena Contreras	Ciudad de México
9	9	12	Milpa Alta	Ciudad de México
9	10	1	Álvaro Obregón	Ciudad de México
9	11	13	Tláhuac	Ciudad de México
9	12	14	Tlalpan	Ciudad de México
9	13	16	Xochimilco	Ciudad de México
9	14	3	Benito Juárez	Ciudad de México
9	15	6	Cuauhtémoc	Ciudad de México
9	16	11	Miguel Hidalgo	Ciudad de México
9	17	15	Venustiano Carranza	Ciudad de México
10	1	5	Canatlán	Canatlán
10	5	1	Durango	Victoria de Durango
10	7	2	Gómez Palacio	Gómez Palacio
10	12	3	Lerdo	Ciudad Lerdo
10	16	9	Nombre de Dios	Nombre de Dios
10	18	12	El Oro	Santa María del Oro
10	20	8	Pánuco de Coronado	Francisco I. Madero
10	21	7	Peñón Blanco	Peñón Blanco
10	23	11	Pueblo Nuevo	El Salto
10	28	6	San Juan del Río	San Juan del Río del Centauro del Norte
10	32	4	Santiago Papasquiaro	Santiago Papasquiaro
10	38	10	Vicente Guerrero	Vicente Guerrero
11	1	24	Abasolo	Abasolo
11	2	1	Acámbaro	Acámbaro
11	3	2	San Miguel de Allende	San miguel de Allende
11	4	33	Apaseo el Alto	Apaseo el Alto
11	5	28	Apaseo el Grande	Apaseo el Grande
11	7	3	Celaya	Celaya
11	7	25	Celaya	Rincón de Tamayo
11	8	34	Manuel Doblado	Ciudad Manuel Doblado
11	9	17	Comonfort	Comonfort
11	9	21	Comonfort	Empalme Escobedo (Escobedo)
11	11	4	Cortazar	Cortazar
11	12	20	Cuerámaro	Cuerámaro
11	13	38	Doctor Mora	Doctor Mora
11	14	39	Dolores Hidalgo Cuna de la Independencia Nacional	Dolores Hgo. Cuna de la Indep. Nal.
11	15	31	Guanajuato	Marfil
11	16	16	Huanímaro	Huanímaro
11	17	6	Irapuato	Irapuato
11	18	35	Jaral del Progreso	Jaral del Progreso
11	19	12	Jerécuaro	Jerécuaro
11	20	7	León	León de los Aldama
11	21	8	Moroleón	Moroleón
11	23	19	Pénjamo	Pénjamo
11	25	29	Purísima del Rincón	Purísima de Bustos
11	26	14	Romita	Romita
11	27	9	Salamanca	Salamanca
11	28	30	Salvatierra	Salvatierra
11	29	36	San Diego de la Unión	San Diego de la Unión
11	31	10	San Francisco del Rincón	San Francisco del Rincón
11	32	32	San José Iturbide	San José Iturbide
11	33	22	San Luis de la Paz	San Luis de la Paz
11	35	37	Santa Cruz de Juventino Rosas	Santa Cruz Juventino Rosas
11	36	13	Santiago Maravatío	Santiago Maravatío
11	38	15	Tarandacuao	Tarandacuao
11	41	18	Uriangato	Uriangato
11	42	23	Valle de Santiago	Valle de Santiago
11	44	26	Villagrán	Villagrán
11	46	27	Yuriria	Yuriria
12	1	1	Acapulco de Juárez	Acapulco de Juárez
12	6	10	Apaxtla	Ciudad Apaxtla de Castrejón
12	7	5	Arcelia	Arcelia
12	11	7	Atoyac de Álvarez	Atoyac de Álvarez
12	12	6	Ayutla de los Libres	Ayutla de los Libres
12	13	30	Azoyú	Azoyú
12	14	9	Benito Juárez	San Jerónimo de Juárez
12	15	12	Buenavista de Cuéllar	Buenavista de Cuellar
12	18	40	Copala	Copala
12	21	16	Coyuca de Benítez	Coyuca de Benítez
12	22	14	Coyuca de Catalán	Coyuca de Catalán
12	23	36	Cuajinicuilapa	Cuajinicuilapa
12	27	13	Cutzamala de Pinzón	Cutzamala de Pinzón
12	28	33	Chilapa de Álvarez	Chilapa de Álvarez
12	29	2	Chilpancingo de los Bravo	Chilpancingo de los Bravo
12	29	39	Chilpancingo de los Bravo	Ocotito
12	30	38	Florencio Villarreal	Cruz Grande
12	33	37	Huamuxtitlán	Huamuxtitlan
12	34	26	Huitzuco de los Figueroa	Huitzuco
12	35	3	Iguala de la Independencia	Iguala de la Independencia
12	38	41	Zihuatanejo de Azueta	Zihuatanejo
12	39	15	Juan R. Escudero	Tierra Colorada
12	45	17	Olinalá	Olinalá
12	48	21	Petatlán	Petatlán
12	50	11	Pungarabato	Ciudad Altamirano
12	52	32	San Luis Acatlán	San Luis Acatlán
12	53	29	San Marcos	San Marcos
12	55	4	Taxco de Alarcón	Taxco de Alarcón
12	57	20	Técpan de Galeana	San Luis de la Loma
12	57	23	Técpan de Galeana	San Luis San Pedro
12	57	25	Técpan de Galeana	Técpan de Galeana
12	58	24	Teloloapan	Teloloapan
12	59	28	Tepecoacuilco de Trujano	Tepecoacuilco de Trujano
12	61	27	Tixtla de Guerrero	Tixtla de Guerrero
12	65	35	Tlalixtaquilla de Maldonado	Tlalixtaquilla
12	66	34	Tlapa de Comonfort	Tlapa de Comonfort
12	67	31	Tlapehuala	Tlapehuala
12	68	22	La Unión de Isidoro Montes de Oca	La Unión
12	75	19	Eduardo Neri	Zumpango del Río
12	77	18	Marquelia	Marquelia
13	3	1	Actopan	Actopan
13	8	2	Apan	Apan
13	28	8	Huejutla de Reyes	Huejutla de Reyes
13	30	12	Ixmiquilpan	Ixmiquilpan
13	48	3	Pachuca de Soto	Pachuca de Soto
13	56	11	Santiago Tulantepec de Lugo Guerrero	Santiago Tulantepec
13	61	4	Tepeapulco	Cd. de Fray Bernardino de Sahagún
13	61	15	Tepeapulco	Tepeapulco
13	63	13	Tepeji del Río de Ocampo	Tepeji del Rio
13	69	10	Tizayuca	Tizayuca
13	74	9	Tlaxcoapan	Tlaxcoapan
13	76	5	Tula de Allende	Tula de Allende
13	76	14	Tula de Allende	Cruz Azul
13	77	6	Tulancingo de Bravo	Tulancingo
13	84	7	Zimapán	Zimapan
14	2	55	Acatlán de Juárez	Acatlán de Juárez
14	3	24	Ahualulco de Mercado	Ahualulco de Mercado
14	6	1	Ameca	Ameca
14	8	20	Arandas	Arandas
14	13	17	Atotonilco el Alto	Atotonilco el Alto
14	15	25	Autlán de Navarro	Autlán de Navarro
14	18	32	La Barca	La Barca
14	21	16	Casimiro Castillo	La Resolana
14	22	36	Cihuatlán	Cihuatlán
14	23	2	Zapotlán el Grande	Ciudad Guzmán
14	24	28	Cocula	Cocula
14	25	35	Colotlán	Colotlán
14	30	46	Chapala	Chapala
14	30	47	Chapala	Ajijic
14	36	22	Etzatlán	Etzatlán
14	37	29	El Grullo	El Grullo
14	39	3	Guadalajara	Guadalajara
14	42	50	Huejuquilla el Alto	Huejuquilla el Alto
14	46	18	Jalostotitlán	Jalostotitlán
14	47	33	Jamay	Jamay
14	50	44	Jocotepec	Jocotepec
14	53	4	Lagos de Moreno	Lagos de Moreno
14	55	26	Magdalena	Magdalena
14	63	5	Ocotlán	Ocotlán
14	66	19	Poncitlán	Poncitlán
14	67	6	Puerto Vallarta	Puerto Vallarta
14	70	42	El Salto	El Quince (San José el Quince)
14	70	43	El Salto	San José el Verde (El Verde)
14	70	53	El Salto	Las Pintitas
14	72	15	San Diego de Alejandría	San Diego de Alejandría
14	73	7	San Juan de los Lagos	San Juan de los Lagos
14	74	27	San Julián	San Julián
14	78	30	San Miguel el Alto	San Miguel el Alto
14	82	23	Sayula	Sayula
14	83	31	Tala	Tala
14	84	21	Talpa de Allende	Talpa de Allende
14	85	54	Tamazula de Gordiano	Tamazula de Gordiano
14	87	45	Tecalitlán	Tecalitlán
14	91	39	Teocaltiche	Teocaltiche
14	93	8	Tepatitlán de Morelos	Tepatitlán de Morelos
14	94	41	Tequila	Tequila
14	97	11	Tlajomulco de Zúñiga	Tlajomulco de Zúñiga
14	98	9	San Pedro Tlaquepaque	Tlaquepaque
14	101	12	Tonalá	Tonalá
14	105	14	Tototlán	Tototlán
14	108	13	Tuxpan	Tuxpan
14	109	52	Unión de San Antonio	Unión de San Antonio
14	111	56	Valle de Guadalupe	Valle de Guadalupe
14	114	38	Villa Corona	Villa Corona
14	116	51	Villa Hidalgo	Villa Hidalgo
14	118	34	Yahualica de González Gallo	Yahualica de González Gallo
14	119	49	Zacoalco de Torres	Zacoalco de Torres
14	120	10	Zapopan	Zapopan
14	121	37	Zapotiltic	Zapotiltic
14	125	48	San Ignacio Cerro Gordo	San Ignacio Cerro Gordo
15	5	39	Almoloya de Juárez	Almoloya de Juárez
15	8	24	Amatepec	Amatepec
15	13	1	Atizapán de Zaragoza	Ciudad Adolfo López Mateos
15	19	28	Capulhuac	Capulhuac
15	20	3	Coacalco de Berriozábal	Coacalco de Berriozabal
15	24	17	Cuautitlán	Cuautitlán
15	25	23	Chalco	Chalco de Díaz Covarrubias
15	29	27	Chicoloapan	San Vicente Chicoloapan de Juárez
15	30	36	Chiconcuac	Chiconcuac
15	31	2	Chimalhuacán	Chimalhuacán
15	33	5	Ecatepec de Morelos	Ecatepec de Morelos
15	37	6	Huixquilucan	Huixquilucan de Degollado
15	39	18	Ixtapaluca	Ixtapaluca
15	50	29	Juchitepec	Juchitepec de Mariano Riva Palacio
15	53	26	Melchor Ocampo	Melchor Ocampo
15	54	8	Metepec	Metepec
15	57	9	Naucalpan de Juárez	Naucalpan de Juárez
15	58	10	Nezahualcóyotl	Ciudad Nezahualcoyotl
15	60	11	Nicolás Romero	Villa Nicolás Romero
15	62	40	Ocoyoacac	Ocoyoacac
15	70	7	La Paz	Los Reyes Acaquilpan (La Paz)
15	76	32	San Mateo Atenco	San Mateo Atenco
15	81	12	Tecámac	Tecamac de Felipe Villanueva
15	82	22	Tejupilco	Tejupilco de Hidalgo
15	95	13	Tepotzotlán	Tepotzotlán
15	96	30	Tequixquiac	Tequixquiac
15	99	19	Texcoco	Texcoco de Mora
15	104	14	Tlalnepantla de Baz	Tlalnepantla de Baz
15	106	20	Toluca	Toluca de Lerdo
15	108	15	Tultepec	Santa Maria Tultepec
15	109	16	Tultitlán	Tultitlán de Mariano Escobedo
15	115	31	Xonacatlán	Xonacatlán
15	120	41	Zumpango	Zumpango
15	121	4	Cuautitlán Izcalli	Cuautitlán Izcalli
15	122	21	Valle de Chalco Solidaridad	Valle de Chalco Solidaridad
16	6	1	Apatzingán	Apatzingán de la Constitución
16	19	25	Cotija	Cotija de la Paz
16	20	27	Cuitzeo	Cuitzeo del Porvenir
16	34	3	Hidalgo	Ciudad Hidalgo
16	38	19	Huetamo	Huetamo de Núñez
16	43	4	Jacona	Jacona de Plancarte
16	45	23	Jiquilpan	Jiquilpan de Juárez
16	50	15	Maravatío	Maravatío de Ocampo
16	52	21	Lázaro Cárdenas	Ciudad Lázaro Cárdenas
16	52	22	Lázaro Cárdenas	Las Guacamayas
16	53	6	Morelia	Morelia
16	55	26	Múgica	Nueva Italia de Ruiz
16	65	13	Paracho	Paracho de Verduzco
16	66	7	Pátzcuaro	Pátzcuaro
16	69	5	La Piedad	La piedad de Cabadas
16	71	17	Puruándiro	Puruándiro
16	75	2	Los Reyes	Los Reyes de Salgado
16	76	8	Sahuayo	Sahuayo de Morelos
16	82	20	Tacámbaro	Tacámbaro de Codallos
16	85	14	Tangancícuaro	Tangancícuaro de Arista
16	98	24	Tuxpan	Tuxpan
16	102	9	Uruapan	Uruapan
16	106	18	Yurécuaro	Yurécuaro
16	107	10	Zacapu	Zacapu
16	108	11	Zamora	Zamora de Hidalgo
16	110	16	Zinapécuaro	Zinapécuaro de Figueroa
16	112	12	Zitácuaro	Heroica Zitácuaro
17	6	1	Cuautla	Cuautla (Cuautla de Morelos)
17	7	2	Cuernavaca	Cuernavaca
17	12	4	Jojutla	Jojutla
17	17	5	Puente de Ixtla	Puente de Ixtla
17	24	6	Tlaltizapán de Zapata	Santa Rosa Treinta
17	25	8	Tlaquiltenango	Tlaquiltenango
17	31	3	Zacatepec	Galeana
17	31	7	Zacatepec	Zacatepec de Hidalgo
18	1	4	Acaponeta	Acaponeta
18	2	18	Ahuacatlán	Ahuacatlán
18	4	6	Compostela	Compostela
18	4	13	Compostela	Las Varas
18	4	16	Compostela	La peñita de Jaltemba
18	6	11	Ixtlán del Río	Ixtlán del Río
18	7	17	Jala	Jala
18	8	14	Xalisco	Xalisco
18	11	9	Ruíz	Ruiz
18	12	10	San Blas	San Blas
18	13	15	San Pedro Lagunillas	San pedro Lagunillas
18	15	1	Santiago Ixcuintla	Santiago Ixcuintla
18	15	8	Santiago Ixcuintla	Villa Hidalgo (El Nuevo)
18	16	5	Tecuala	Tecuala
18	17	2	Tepic	Tepic
18	17	7	Tepic	Francisco I. Madero (Puga)
18	18	3	Tuxpan	Tuxpan
18	20	12	Bahía de Banderas	Bucerías
19	5	20	Anáhuac	Anáhuac
19	6	1	Apodaca	Ciudad Apodaca
19	9	15	Cadereyta Jiménez	Cadereyta Jiménez
19	12	12	Ciénega de Flores	Ciénega de Flores
19	14	11	Doctor Arroyo	Doctor Arroyo
19	18	21	García	García
19	19	2	San Pedro Garza García	San Pedro Garza García
19	21	3	General Escobedo	Ciudad General Escobedo
19	26	4	Guadalupe	Guadalupe
19	29	13	Hualahuises	Hualahuises
19	31	22	Juárez	Ciudad Benito Juárez
19	33	5	Linares	Linares
19	38	6	Montemorelos	Montemorelos
19	39	7	Monterrey	Monterrey
19	44	8	Sabinas Hidalgo	Ciudad Sabinas Hidalgo
19	46	9	San Nicolás de los Garza	San Nicolás de los Garza
19	48	10	Santa Catarina	Ciudad Santa Catarina
19	49	17	Santiago	Santiago
19	49	18	Santiago	El cercado
20	2	33	Acatlán de Pérez Figueroa	Vicente Camalote
20	6	52	Asunción Nochixtlán	Asunción Nochixtlán
20	10	48	El Barrio de la Soledad	Lagunas
20	14	49	Ciudad Ixtepec	Ciudad Ixtepec
20	21	15	Cosolapa	Cosolapa
20	23	56	Cuilápam de Guerrero	Cuilápam de Guerrero
20	25	30	Chahuites	Chahuites
20	28	31	Heroica Ciudad de Ejutla de Crespo	Heroica Ciudad de Ejutla de Crespo
20	39	6	Heroica Ciudad de Huajuapan de León	Heroica Ciudad de Huajuapan de León
20	43	1	Heroica Ciudad de Juchitán de Zaragoza	Juchitán (Juchitán de Zaragoza)
20	44	7	Loma Bonita	Loma Bonita
20	55	42	Mariscala de Juárez	Mariscala de Juárez
20	57	5	Matías Romero Avendaño	Matías Romero Avendaño
20	59	25	Miahuatlán de Porfirio Díaz	Miahuatlán de Porfirio Díaz
20	62	18	Natividad	Natividad
20	64	27	Nejapa de Madero	El Camarón
20	67	2	Oaxaca de Juárez	Oaxaca de Juárez
20	68	23	Ocotlán de Morelos	Ocotlán de Morelos
20	73	14	Putla Villa de Guerrero	Putla Villa de Guerrero
20	79	3	Salina Cruz	Salina Cruz
20	107	59	San Antonio de la Cal	San Antonio de la Cal
20	124	54	San Blas Atempa	San Blas Atempa
20	134	12	San Felipe Jalapa de Díaz	San Felipe Jalapa de Díaz
20	143	53	San Francisco Ixhuatán	San Francisco Ixhuatán
20	150	41	San Francisco Telixtlahuaca	San Francisco Telixtlahuaca
20	177	21	San Juan Bautista Cuicatlán	San Juan Bautista Cuicatlán
20	180	35	San Juan Bautista Lo de Soto	San Juan Bautista lo de Soto
20	184	4	San Juan Bautista Tuxtepec	San Juan Bautista Tuxtepec
20	185	36	San Juan Cacahuatepec	San Juan Cacahuatepec
20	269	38	San Miguel el Grande	San Miguel el Grande
20	277	22	Villa Sola de Vega	Villa Sola de Vega
20	294	40	San Pablo Huitzo	San Pablo Huitzo
20	298	17	San Pablo Villa de Mitla	San Pablo Villa de Mitla
20	318	8	San Pedro Mixtepec -Dto. 22 -	Puerto Escondido
20	318	28	San Pedro Mixtepec -Dto. 22 -	San Pedro Mixtepec -Dto. 22-
20	324	45	San Pedro Pochutla	San Pedro Pochutla
20	327	32	San Pedro Tapanatepec	San Pedro Tapanatepec
20	333	37	San Pedro Totolápam	San Pedro Totolapa
20	334	9	Villa de Tututepec de Melchor Ocampo	Río Grande o Piedra Parada
20	348	51	San Sebastián Tecomaxtlahuaca	San Sebastián Tecomaxtlahuaca
20	350	57	San Sebastián Tutla	El Rosario
20	377	29	Santa Cruz Itundujia	Santa Cruz Itundujia
20	390	58	Santa Lucía del Camino	Santa Lucia del Camino
20	397	46	Heroica Ciudad de Tlaxiaco	Heroica Ciudad de Tlaxiaco
20	413	13	Santa María Huatulco	Bahias de Huatulco
20	413	20	Santa María Huatulco	Santa María Huatulco
20	467	44	Santiago Jamiltepec	Santiago Jamiltepec
20	469	50	Santiago Juxtlahuaca	Santiago Juxtlahuaca
20	482	43	Santiago Pinotepa Nacional	Santiago Pinotepa Nacional
20	483	11	Santiago Suchilquitongo	Santiago Suchilquitongo
20	515	55	Santo Domingo Tehuantepec	Santo Domingo Tehuantepec
20	540	34	Villa de Tamazulápam del Progreso	Villa de Tamazulápam del Progreso
20	545	19	Teotitlán de Flores Magón	Teotitlán de Flores Magón
20	551	16	Tlacolula de Matamoros	Tlacolula de Matamoros
20	557	26	Unión Hidalgo	Unión Hidalgo
20	559	47	San Juan Bautista Valle Nacional	San Juan Bautista Valle Nacional
20	565	24	Villa de Zaachila	Villa de Zaachila
20	570	39	Zimatlán de Álvarez	Zimatlán de Álvarez
21	3	10	Acatlán	Acatlán de Osorio
21	15	17	Amozoc	Amozoc
21	19	1	Atlixco	Atlixco
21	41	11	Cuautlancingo	Cuautlancingo
21	45	16	Chalchicomula de Sesma	Ciudad Serdán
21	71	9	Huauchinango	Huauchinango
21	85	2	Izúcar de Matamoros	Izúcar de Matamoros
21	114	3	Puebla	Puebla (Heroica Puebla)
21	119	7	San Andrés Cholula	San Andrés Cholula
21	132	4	San Martín Texmelucan	San Martín Texmelucan de Labastida
21	140	8	San Pedro Cholula	San Pedro Cholula
21	154	13	Tecamachalco	Tecamachalco
21	156	5	Tehuacán	Tehuacan
21	164	12	Tepeaca	Tepeaca
21	174	6	Teziutlán	Teziutlan
21	197	15	Xicotepec	Xicotepec
21	208	14	Zacatlán	Zacatlán
22	6	4	Corregidora	El Pueblito
22	14	1	Querétaro	Querétaro
22	16	2	San Juan del Río	San Juan del Rio
23	1	2	Cozumel	Cozumel
23	2	3	Felipe Carrillo Puerto	Felipe Carrillo Puerto
23	3	7	Isla Mujeres	Isla Mujeres
23	5	1	Benito Juárez	Cancún
23	7	6	Lázaro Cárdenas	Kantunilkín
23	8	5	Solidaridad	Playa del Carmen
23	10	8	Bacalar	Bacalar
24	5	9	Cárdenas	Cárdenas
24	7	14	Cedral	Cedral
24	8	10	Cerritos	Cerritos
24	10	13	Ciudad del Maíz	Ciudad del Maíz
24	11	17	Ciudad Fernández	Fracción el Refugio
24	13	1	Ciudad Valles	Ciudad Valles
24	15	7	Charcas	Charcas
24	16	2	Ebano	Ébano
24	20	3	Matehuala	Matehuala
24	24	4	Rioverde	Rioverde
24	25	8	Salinas	Salinas de Hidalgo
24	28	5	San Luis Potosí	San Luis Potosí
24	32	19	Santa María del Río	Santa María del Río
24	35	6	Soledad de Graciano Sánchez	Soledad de Graciano Sánchez
24	36	12	Tamasopo	Tamasopo
24	37	18	Tamazunchale	Tamazunchale
24	40	11	Tamuín	Tamuin
24	43	15	Tierra Nueva	Tierra Nueva
24	50	16	Villa de Reyes	Villa de Reyes
24	58	20	El Naranjo	El Naranjo
25	1	1	Ahome	Los Mochis
25	1	9	Ahome	Ahome
25	1	10	Ahome	Higuera de Zaragoza
25	1	23	Ahome	Topolobampo
25	2	15	Angostura	Angostura
25	5	21	Cosalá	Cosalá
25	6	2	Culiacán	Culiacán Rosales
25	6	8	Culiacán	Quila
25	6	20	Culiacán	Aguaruto
25	7	11	Choix	Choix
25	8	17	Elota	La Cruz
25	9	3	Escuinapa	Escuinapa de Hidalgo
25	10	16	El Fuerte	San Blas
25	11	4	Guasave	Guasave
25	12	5	Mazatlán	Mazatlán
25	12	12	Mazatlán	Villa Unión
25	13	14	Mocorito	Mocorito
25	14	18	Rosario	El rosario
25	15	6	Salvador Alvarado	Guamúchil
25	16	22	San Ignacio	San Ignacio
25	17	13	Sinaloa	Sinaloa de Leyva
25	17	19	Sinaloa	Estación Naranjo
25	18	7	Navolato	Navolato
25	18	24	Navolato	Lic. Benito Juárez (Campo Gobierno)
26	2	2	Agua Prieta	Agua Prieta
26	17	3	Caborca	Heroica Caborca
26	18	4	Cajeme	Ciudad Obregón
26	19	12	Cananea	Heroica Ciudad de Cananea
26	25	5	Empalme	Empalme
26	29	6	Guaymas	Heroica Guaymas
26	30	7	Hermosillo	Hermosillo
26	33	8	Huatabampo	Huatabampo
26	36	14	Magdalena	Magdalena de Kino
26	42	9	Navojoa	Navojoa
26	43	10	Nogales	Heroica Nogales
26	48	11	Puerto Peñasco	Puerto Peñasco
26	55	1	San Luis Río Colorado	San Luis Río Colorado
26	70	13	General Plutarco Elías Calles	Sonoita
27	2	1	Cárdenas	Cárdenas
27	3	11	Centla	Frontera
27	4	2	Centro	Villahermosa
27	5	3	Comalcalco	Comalcalco
27	6	12	Cunduacán	Cunduacán
27	7	4	Emiliano Zapata	Emiliano Zapata
27	8	13	Huimanguillo	Huimanguillo
27	10	5	Jalpa de Méndez	Jalpa de Méndez
27	12	7	Macuspana	Macuspana
27	14	10	Paraíso	Paraíso
27	16	9	Teapa	Teapa
27	17	8	Tenosique	Tenosique de Pino Suárez
28	3	1	Altamira	Altamira
28	7	2	Camargo	Ciudad Camargo
28	9	3	Ciudad Madero	Ciudad Madero
28	12	12	González	González
28	12	15	González	Estación Manuel (Úrsulo Galván)
28	14	20	Guerrero	Nueva Ciudad Guerrero
28	15	14	Gustavo Díaz Ordaz	Ciudad Gustavo Díaz Ordaz
28	17	13	Jaumave	Jaumave
28	21	4	El Mante	Ciudad Mante
28	22	5	Matamoros	Heroica Matamoros
28	25	17	Miguel Alemán	Ciudad Miguel Alemán
28	27	6	Nuevo Laredo	Nuevo Laredo
28	32	7	Reynosa	Reynosa
28	33	8	Río Bravo	Ciudad Río Bravo
28	35	9	San Fernando	San Fernando
28	37	18	Soto la Marina	Soto la Marina
28	38	10	Tampico	Tampico
28	39	19	Tula	Ciudad Tula
28	40	21	Valle Hermoso	Valle Hermoso
28	41	11	Victoria	Ciudad Victoria
28	43	16	Xicoténcatl	Xicoténcatl
29	5	1	Apizaco	Apizaco
29	6	5	Calpulalpan	Calpulalpan
29	10	6	Chiautempan	Chiautempan
29	13	4	Huamantla	Huamantla
29	25	2	San Pablo del Monte	Villa Vicente Guerrero
30	3	1	Acayucan	Acayucan
30	10	39	Altotonga	Altotonga
30	11	34	Alvarado	Alvarado
30	13	2	Naranjos Amatlán	Naranjos
30	14	37	Amatlán de los Reyes	Paraje Nuevo
30	15	50	Angel R. Cabada	Ángel R. Cabada
30	16	35	La Antigua	José Cardel
30	21	48	Atoyac	General Miguel Alemán (Potrero Nuevo)
30	21	56	Atoyac	Atoyac
30	26	36	Banderilla	Banderilla
30	28	3	Boca del Río	Boca del RÍo
30	32	45	Catemaco	Catemaco
30	33	58	Cazones de Herrera	Cazones de Herrera
30	34	61	Cerro Azul	Cerro Azul
30	38	4	Coatepec	Coatepec
30	39	6	Coatzacoalcos	Coatzacoalcos
30	40	49	Coatzintla	Coatzintla
30	44	7	Córdoba	Córdoba
30	45	19	Cosamaloapan de Carpio	Cosamaloapan
30	48	53	Cosoleacaque	Cosoleacaque
30	53	32	Cuitláhuac	Cuitláhuac
30	61	47	Las Choapas	Las Choapas
30	68	33	Fortín	Fortín de las Flores
30	69	25	Gutiérrez Zamora	Gutiérrez Zamora
30	71	28	Huatusco	Huatusco de Chicuellar
30	72	43	Huayacocotla	Huayacocotla
30	73	40	Hueyapan de Ocampo	Juan Díaz Covarrubias
30	74	57	Huiloapan de Cuauhtémoc	Huiloapan de Cuauhtémoc
30	77	31	Isla	Isla
30	85	29	Ixtaczoquitlán	Ixtaczoquitlán
30	87	10	Xalapa	Xalapa-Enríquez
30	89	9	Jáltipan	Jáltipan de Morelos
30	94	27	Juan Rodríguez Clara	Juan Rodríguez Clara
30	97	54	Lerdo de Tejada	Lerdo de Tejada
30	108	11	Minatitlán	Minatitlán
30	111	41	Moloacán	Cuichapa
30	115	46	Nogales	Nogales
30	118	12	Orizaba	Orizaba
30	123	21	Pánuco	Pánuco
30	124	13	Papantla	Papantla de Olarte
30	125	68	Paso del Macho	Paso del Macho
30	126	44	Paso de Ovejas	Paso de Ovejas
30	129	26	Platón Sánchez	Platón Sánchez
30	130	38	Playa Vicente	Playa Vicente
30	131	14	Poza Rica de Hidalgo	Poza Rica de Hidalgo
30	138	30	Río Blanco	Río Blanco
30	141	15	San Andrés Tuxtla	San Andrés Tuxtla
30	141	66	San Andrés Tuxtla	Sihuapan
30	143	42	Santiago Tuxtla	Santiago Tuxtla
30	148	60	Soledad de Doblado	Soledad de Doblado
30	152	22	Tampico Alto	Tampico Alto
30	155	24	Tantoyuca	Tantoyuca
30	161	23	Tempoal	Tempoal de Sánchez
30	173	62	Tezonapa	Tezonapa
30	174	18	Tierra Blanca	Tierra Blanca
30	175	55	Tihuatlán	Tihuatlán
30	176	52	Tlacojalpan	Tlacojalpan
30	183	69	Tlapacoyan	Tlapacoyan
30	189	16	Tuxpan	Túxpam de Rodríguez Cano
30	193	17	Veracruz	Veracruz
30	197	59	Yecuatla	Yecuatla
30	204	5	Agua Dulce	Agua dulce
30	205	67	El Higo	El Higo
30	207	8	Tres Valles	Tres Valles
30	208	20	Carlos A. Carrillo	Carlos A. Carrillo
30	211	51	San Rafael	San Rafael
31	41	6	Kanasín	Kanasín
31	50	1	Mérida	Mérida
31	52	4	Motul	Motul de Carrillo Puerto
31	89	3	Ticul	Ticul
31	96	2	Tizimín	Tizimín
31	102	5	Valladolid	Valladolid
32	5	12	Calera	Víctor Rosales
32	8	6	Cuauhtémoc	Ciudad Cuauhtémoc
32	10	1	Fresnillo	Fresnillo
32	17	4	Guadalupe	Guadalupe
32	19	17	Jalpa	Jalpa
32	20	2	Jerez	Jerez de García Salinas
32	22	19	Juan Aldama	Juan Aldama
32	24	18	Loreto	Loreto
32	25	14	Luis Moya	Luis Moya
32	33	15	Moyahua de Estrada	Moyahua de Estrada
32	34	11	Nochistlán de Mejía	Nochistlán de Mejía
32	36	7	Ojocaliente	Ojocaliente
32	39	5	Río Grande	Río Grande
32	42	16	Sombrerete	Sombrerete
32	49	13	Valparaíso	Valparaíso
32	51	10	Villa de Cos	Villa de Cos
32	54	8	Villa Hidalgo	Villa Hidalgo
32	55	9	Villanueva	Villanueva
