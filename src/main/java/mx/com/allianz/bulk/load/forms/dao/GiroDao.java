package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Giro;
import mx.com.allianz.bulk.load.forms.models.GiroPK;

@Transactional
public interface GiroDao extends CrudRepository<Giro, GiroPK> {

}
