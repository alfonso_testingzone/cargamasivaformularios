package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;


public class EstadoPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7961974616143888547L;

	protected Integer codigoPais;
	protected Integer codigoEstado;
	
	public EstadoPK() {
	}

	public EstadoPK(Integer codigoPais, Integer codigoEstado) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
	}

	public Integer getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(Integer codigoPais) {
		this.codigoPais = codigoPais;
	}

	public Integer getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	@Override
	public int hashCode() {
		return codigoPais * 1_000_000 + codigoEstado * 10_000 ;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
	        return false;
	    }
	    
	    final EstadoPK other = (EstadoPK) obj;
	   
	    if (this.codigoPais == null || other.codigoPais == null ||
	    		this.codigoEstado == null || other.codigoEstado == null){
	    	return false;
	    }
	    
	    if (this.codigoPais != other.codigoPais ||
	    		this.codigoEstado != other.codigoEstado ) {
    		return false;
    	}
	    
	    return true;
	}
	
	
}
