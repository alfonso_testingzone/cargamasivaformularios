package mx.com.allianz.bulk.load.forms.io;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {
	public static final SimpleDateFormat FORMATER = new SimpleDateFormat("dd-MMM-yyyy");

	public static Date convertirCadenaFecha(String fechaCadena) {
		Date fecha = null;
		try {
			fecha = FORMATER.parse(fechaCadena);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return fecha;
	}

}
