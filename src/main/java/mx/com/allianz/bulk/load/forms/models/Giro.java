package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="PTL_CAT_GIRO_SAT")
@IdClass(GiroPK.class)
public class Giro {

	@Id
	@Column(name="CODIGO_GIRO")
	private int codigoGiro;
	
	@Id
	@Column(name="CODIGO_ACTIVIDAD")
	private int codigoActividad;
	
	@Column(name="DESCRIPCION_GIRO")
	private String descripcionGiro;

	
	public Giro() {
	}

	
	public Giro(String row) {
		String[] params = row.split("\\t");
		this.codigoGiro = Integer.parseInt(params[0]);
		this.codigoActividad = Integer.parseInt(params[1]);
		this.descripcionGiro = params[2];
	}

	public Giro(int codigoGiro, int codigoActividad, String descripcionGiro) {
		super();
		this.codigoGiro = codigoGiro;
		this.codigoActividad = codigoActividad;
		this.descripcionGiro = descripcionGiro;
	}


	public int getCodigoGiro() {
		return codigoGiro;
	}


	public void setCodigoGiro(int codigoGiro) {
		this.codigoGiro = codigoGiro;
	}


	public int getCodigoActividad() {
		return codigoActividad;
	}


	public void setCodigoActividad(int codigoActividad) {
		this.codigoActividad = codigoActividad;
	}


	public String getDescripcionGiro() {
		return descripcionGiro;
	}


	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}


	@Override
	public String toString() {
		return "Giro [codigoGiro=" + codigoGiro + ", codigoActividad=" + codigoActividad + ", descripcionGiro="
				+ descripcionGiro + "]";
	}
	
	
}
