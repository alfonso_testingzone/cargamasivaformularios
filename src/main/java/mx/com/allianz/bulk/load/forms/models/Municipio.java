package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PTL_CAT_MUNICIPIO_TR")
@IdClass(MunicipioPK.class)
public class Municipio {
	
	@Id
	@Column(name="COD_PAIS")
	private int codigoPais;
	
	@Id
	@Column(name="COD_ESTADO")
	private int codigoEstado;
	
	@Id
	@Column(name="COD_CIUDAD")
	private int codigoCiudad;
	
	@Id
	@Column(name="COD_MUNICIPIO")
	private int codigoMunicipio;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	public Municipio() {
		
	}

	public Municipio(int codigoPais, int codigoEstado, int codigoCiudad, int codigoMunicipio, String descripcion) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
		this.codigoMunicipio = codigoMunicipio;
		this.descripcion = descripcion;
	}
	
	public Municipio(String row) {
		String[] params = row.split("\\t");
		this.codigoPais = Integer.parseInt(params[0]);
		this.codigoEstado = Integer.parseInt(params[1]);
		this.codigoCiudad = Integer.parseInt(params[2]);
		this.codigoMunicipio = Integer.parseInt(params[3]);
		this.descripcion = params[4];
	}

	public int getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(int codigoPais) {
		this.codigoPais = codigoPais;
	}

	public int getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public int getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(int codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public int getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(int codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;

	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Municipio [codigoMunicipio=" + codigoMunicipio + ", codigoPais=" + codigoPais + ", codigoEstado="
				+ codigoEstado + ", codigoCiudad=" + codigoCiudad + ", descripcion=" + descripcion + "]";
	}

	
}
