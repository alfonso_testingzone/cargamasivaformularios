package mx.com.allianz.bulk.load.forms.io;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import mx.com.allianz.bulk.load.forms.dao.ActividadDao;
import mx.com.allianz.bulk.load.forms.dao.CiudadDao;
import mx.com.allianz.bulk.load.forms.dao.ClasificadorDao;
import mx.com.allianz.bulk.load.forms.dao.DocumentosDao;
import mx.com.allianz.bulk.load.forms.dao.EstadoDao;
import mx.com.allianz.bulk.load.forms.dao.EstatusDao;
import mx.com.allianz.bulk.load.forms.dao.GiroDao;
import mx.com.allianz.bulk.load.forms.dao.GrupoPagoDao;
import mx.com.allianz.bulk.load.forms.dao.HospitalDao;
import mx.com.allianz.bulk.load.forms.dao.MonedaDao;
import mx.com.allianz.bulk.load.forms.dao.MunicipioDao;
import mx.com.allianz.bulk.load.forms.dao.PaisDao;
import mx.com.allianz.bulk.load.forms.dao.ProductoDao;
import mx.com.allianz.bulk.load.forms.dao.ProveedorDao;
import mx.com.allianz.bulk.load.forms.dao.QuejaDao;
import mx.com.allianz.bulk.load.forms.dao.QuejaRefiereDao;
import mx.com.allianz.bulk.load.forms.dao.QuejaRelacionadaDao;
import mx.com.allianz.bulk.load.forms.dao.SubgrupoDao;
import mx.com.allianz.bulk.load.forms.dao.SucursalDao;
import mx.com.allianz.bulk.load.forms.models.Actividad;
import mx.com.allianz.bulk.load.forms.models.Ciudad;
import mx.com.allianz.bulk.load.forms.models.Clasificador;
import mx.com.allianz.bulk.load.forms.models.Documento;
import mx.com.allianz.bulk.load.forms.models.Estado;
import mx.com.allianz.bulk.load.forms.models.Estatus;
import mx.com.allianz.bulk.load.forms.models.Giro;
import mx.com.allianz.bulk.load.forms.models.GrupoPago;
import mx.com.allianz.bulk.load.forms.models.Hospital;
import mx.com.allianz.bulk.load.forms.models.Moneda;
import mx.com.allianz.bulk.load.forms.models.Municipio;
import mx.com.allianz.bulk.load.forms.models.Pais;
import mx.com.allianz.bulk.load.forms.models.Producto;
import mx.com.allianz.bulk.load.forms.models.Proveedor;
import mx.com.allianz.bulk.load.forms.models.Queja;
import mx.com.allianz.bulk.load.forms.models.QuejaRefiere;
import mx.com.allianz.bulk.load.forms.models.QuejaRelacionada;
import mx.com.allianz.bulk.load.forms.models.Subgrupo;
import mx.com.allianz.bulk.load.forms.models.Sucursal;

@Service
public class ClientTxtImpl implements ClientTxt{
	public static final String PATH_TXT = "src/main/resources/txt/";

	@Autowired
	private PaisDao paisDao;
	
	@Autowired
	private EstadoDao estadoDao;

	@Autowired
	private CiudadDao ciudadDao;

	@Autowired
	private MunicipioDao municipioDao;
	
	@Autowired
	private ClasificadorDao clasificadorDao;
	
	@Autowired
	private DocumentosDao documentosDao;
	
	@Autowired
	private GrupoPagoDao grupoPagoDao;
	
	@Autowired
	private HospitalDao hospitalDao;
	
	@Autowired
	private MonedaDao monedaDao;
	
	@Autowired
	private SucursalDao sucursalDao;
	
	@Autowired
	private ProductoDao productoDao;
	
	@Autowired
	private QuejaRefiereDao quejaRefiereDao;
	
	@Autowired
	private QuejaRelacionadaDao quejaRelacionadaDao;
	
	@Autowired
	private QuejaDao quejaDao;
	
	@Autowired
	private SubgrupoDao subgrupoDao;
	
	@Autowired
	private ProveedorDao proveedorDao;
	
	@Autowired
	private EstatusDao estatusDao;
	
	@Autowired
	private ActividadDao actividadDao;
	
	@Autowired
	private GiroDao giroDao;
	
	public <T,ID extends Serializable> void cargarObjeto(String fileName, CrudRepository<T,ID> dao, Function<String, T> f){
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			dao.save(stream.map(f).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void cargarTodo() {
//		cargarObjeto("PTL_CAT_PAIS_TR.txt", paisDao, Pais::new);
//		cargarObjeto("PTL_CAT_ESTADO_TR.txt", estadoDao, Estado::new);
//		cargarObjeto("PTL_CAT_CIUDAD_TR.txt", ciudadDao, Ciudad::new);
//		cargarObjeto("PTL_CAT_MUNICIPIO_TR.txt", municipioDao, Municipio::new);
//		cargarObjeto("PTL_SUCURSAL_TR.txt", sucursalDao, Sucursal::new);
//		cargarObjeto("CATALOGO_MONEDA.txt", monedaDao, Moneda::new);
//		cargarObjeto("CLASIFICADOR.txt", clasificadorDao, Clasificador::new);
//		cargarObjeto("ProdACX.txt", productoDao, Producto::new);
//		cargarObjeto("PTL_CAT_HOSPITAL_TR.txt", hospitalDao, Hospital::new);
//		cargarObjeto("PTL_CATALOGO_DOCUMENTOS_TR.txt", documentosDao, Documento::new);
//		cargarObjeto("PTL_GRUPOS_PAGO_TR.txt", grupoPagoDao, GrupoPago::new);
//		cargarObjeto("QJA_RELACIONADA.txt", quejaRelacionadaDao, QuejaRelacionada::new);
//		cargarObjeto("QJA_REFIERE.txt", quejaRefiereDao, QuejaRefiere::new);
		cargarObjeto("PTL_SUBGRUPOS_PAGO_TR.txt", subgrupoDao, Subgrupo::new);
//		cargarQueja();
//		cargarSubgrupo(); // Se sustituye por carga completa desde archivo
//		cargarProveedor();
//		cargaEstatus();
		
		// Ya no se requieren en Formularios
//		cargarEvaluacion();
//		cargarSolicitud();
		
//		cargarObjeto("PTL_CAT_NACIONALIDAD.txt", nacionalidadDao, Nacionalidad::new);
//		cargarObjeto("PTL_CAT_PARENTESCO.txt", parentescoDao, Parentesco::new);
//		cargarObjeto("sepomex_estado_municipio_utf16.txt", sepomexEstadoMunicipioDao, SepomexEstadoCiudadMunicipio::new);
//		cargarObjeto("cat_sepomex_utf16.txt", sepomexDao, Sepomex::new);
//		cargarActividad();
//		cargarGiro();
		
	}
	


	public void cargarActividad(){
		Actividad actividad = new Actividad(5464,"Evaluacion Documentos" );
		actividadDao.save(actividad);
				
	} 
	
	public void cargarGiro(){
		Giro giro = new Giro(5464 , 666 ,"Evaluacion Documentos" );
		giroDao.save(giro);
				
	} 
	
	public void cargaEstatus(){	
		Estatus estatus = new Estatus(((int)(Math.random()*100)),((int)(Math.random()*100)),new Date(),"Validando","Tramite en proceso de validacon",55543);
		
		estatusDao.save(estatus);
		
	}
	
	public void cargarProveedor(){
		Proveedor proveedor = new Proveedor(((int)(Math.random()*100)),"TestProv","moral","ROCG900916JR5","ROCG900916HDFDRV00",
				"Tester S.A de C.V","Test2 yuju","govanni","Roco","Cruz",
				"16-septiembre-90","viejaConfiable","065","34567765","34567808",
				"5533224455","pepe@gmail.com","www.teste2.com.mx","Mar Marmara","12345",
				"65","Cuahutemoc","543321","nolase","Mexico",
				"Mexico","Mexico",true,true,"Bancomer","123456789098643",
				"555778hhj",true,"Bancomer Esp","Bancomer españa");
			proveedorDao.save(proveedor);
		
	}
	
	public void cargarSubgrupo(){
		Subgrupo subgrupo = new  Subgrupo(((int)(Math.random()*100)),"5565bmx","556865-bmx","Banco bmx de mexico",
				"492","14","234","534","Calle las palmas por que esta bien",
				"Giovanni","55674879","54123","narvarte","589uuh","66",
				"2","543hy");
		subgrupoDao.save(subgrupo);
	}
	
	public void cargarQueja(){
			Queja queja = new  Queja(("Folio-"+((int)Math.random()*100)),
					"Queja",2015,1122,"Cliente","12345",
					"Giovanni","Rodriguez","ROCG900916JR5","56789043","5546334231",
					"YUJUUU@gmail.com","Jalisco","Otros","Otros",
					"Los datos ingresados estan bien",
					"archivo.txt",new Date(),"Quejas",
					"Allianz34567","Seguros","Agente","CVL-342","CVL-556767","Prueba S.A de C.V");
			quejaDao.save(queja);
	}
	
	public void cargarPaises(String fileName){
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			paisDao.save(stream.map(Pais::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void cargarEstados(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			estadoDao.save(stream.map(Estado::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarCiudades(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			ciudadDao.save(stream.map(Ciudad::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarMunicipios(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			municipioDao.save(stream.map(Municipio::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void cargarSucursales(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			sucursalDao.save(stream.map(Sucursal::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void cargarMonedas(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			monedaDao.save(stream.map(Moneda::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarClasificador(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			clasificadorDao.save(stream.map(Clasificador::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarHospitales(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			hospitalDao.save(stream.map(Hospital::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarDocumentos(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			documentosDao.save(stream.map(Documento::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarGruposPago(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			grupoPagoDao.save(stream.map(GrupoPago::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void cargarProductos(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			productoDao.save(stream.map(Producto::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarQuejasRefiere(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			quejaRefiereDao.save(stream.map(QuejaRefiere::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cargarQuejasRelacionada(String fileName) {
		System.out.println("Entré al método, filename:"+fileName);
		try (Stream<String> stream = Files.lines(Paths.get(PATH_TXT+fileName), Charset.forName("UTF-16")) ) {
			quejaRelacionadaDao.save(stream.map(QuejaRelacionada::new).collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
