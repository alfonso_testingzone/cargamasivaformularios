package mx.com.allianz.bulk.load.forms.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PTL_CAT_PARENTESCO")
public class Parentesco {
	@Id
	@Column(name="TIPO_PARENTESCO")
	private String tipoParentesco;
	
	@Column(name="SUB_TIPO_PARENTESCO")
	private int subTipoParentesco;
	
	
	public Parentesco() {
		
	}

	public Parentesco(String tipoParentesco, int subTipoParentesco) {
		this.tipoParentesco = tipoParentesco;
		this.subTipoParentesco = subTipoParentesco;
	}
	
	public Parentesco(String row) {
		String[] params = row.split("\\t");
		this.tipoParentesco = params[1];
		this.subTipoParentesco = Integer.parseInt(params[0]);
	}

	public String getTipoParentesco() {
		return tipoParentesco;
	}

	public void setTipoParentesco(String tipoParentesco) {
		this.tipoParentesco = tipoParentesco;
	}

	public int getSubTipoParentesco() {
		return subTipoParentesco;
	}

	public void setSubTipoParentesco(int subTipoParentesco) {
		this.subTipoParentesco = subTipoParentesco;
	}

	@Override
	public String toString() {
		return "Parentesco [tipoParentesco=" + tipoParentesco + ", subTipoParentesco=" + subTipoParentesco + "]";
	}

	
	

}
