package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web
 */

@XmlRootElement
@Entity
@Table(name = "PTL_SUCURSAL_TR")
public class Sucursal {

	//Atributos de la clase
	@Id
	@Column(name="ID_SUCURSAL")
	private  int idSucursal;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	
	//Constructor default
	public Sucursal() {
		
	}
	
	//Constructor sobrecargado
	public Sucursal(int idSucursal, String descripcion) {
		this.idSucursal = idSucursal;
		this.descripcion = descripcion;
	}
	
	public Sucursal(String row) {
		String[] params = row.split("\\t");
		this.idSucursal = Integer.parseInt(params[0]);
		this.descripcion = params[1];
	}
	
	//Getters & Setters
	public int getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "Sucursal [idSucursal=" + idSucursal  + ", descripcion=" + descripcion
				+ "]";
	}
	
	
}