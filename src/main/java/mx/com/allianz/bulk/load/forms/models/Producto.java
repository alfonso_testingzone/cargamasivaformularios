package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="PTL_CAT_PRODUCTO_TR")
public class Producto {
	@Id
	@Column(name="CODPROD")
	private String codigoProducto;
	
	@Column(name="DESCPROD")
	private String descripcionProducto;
	
	@Column(name="STSPROD")
	private String tipoProducto;

	public Producto() {
	}
	
	public Producto(String codigoProducto, String descripcionProducto, String tipoProducto) {
		this.codigoProducto = codigoProducto;
		this.descripcionProducto = descripcionProducto;
		this.tipoProducto = tipoProducto;
	}
	
	public Producto(String row) {
		String[] params = row.split("\\t");
		this.codigoProducto = params[0];
		this.descripcionProducto = params[1];
		this.tipoProducto = params[2];
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	
	
}
