package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00edz 
 *  que se puede serializar y devolver  
 *  los servicios web
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_CAT_CLASIFICADOR")
public class Clasificador {
	
	//Atributos de la clase
	@Id
	@Column(name="ID_CLASIFICADOR")
	private int idClasificador;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	//Constructor Default
	public Clasificador() {
		
	}
	
	public Clasificador(String row) {
		String[] params = row.split("\\t");
		this.idClasificador = Integer.parseInt(params[0]);
		this.nombre = params[1];
		this.descripcion = params[1];

	}
	
	//Constructor sobrecargado
	
	public Clasificador(int idClasificador, String nombre, String descripcion) {
		this.idClasificador = idClasificador;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	//Getters & Setters
	
	public int getIdClasificador() {
		return idClasificador;
	}

	public void setIdClasificador(int idClasificador) {
		this.idClasificador = idClasificador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	@Override
	public String toString() {
		return "Clasificador [idClasificador=" + idClasificador + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ "]";
	}
	
	

}