package mx.com.allianz.bulk.load.forms.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement
@Entity
@Table(name = "PTL_CAT_PAIS_TR")
public class Pais {
	
	@Id
	@Column(name="COD_PAIS")
	private int codigoPais;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	//Constructor default.
	public Pais() { }
	
	//Constructor sobrecargado.
	public Pais(int codigoPais, String descripcion) {
		this.codigoPais = codigoPais;
		this.descripcion = descripcion;
	}

	public Pais(String row) {
		String[] params = row.split("\\t");
		this.codigoPais = Integer.parseInt(params[0]);
		this.descripcion = params[1];
	}

	//Getters & Setters.
	public int getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(int codigoPais) {
		this.codigoPais = codigoPais;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	@Override
	public String toString() {
		return "Pais [codigoPais=" + codigoPais + ", descripcion=" + descripcion + "]";
	}
	
}
