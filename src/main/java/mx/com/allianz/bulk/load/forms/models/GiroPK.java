package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;

public class GiroPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4282548218678497076L;
	
	protected Integer codigoGiro;
	protected Integer codigoActividad;
	
	public GiroPK() {
	}

	public GiroPK(Integer codigoGiro, Integer codigoActividad) {
		this.codigoGiro = codigoGiro;
		this.codigoActividad = codigoActividad;
	}

	public Integer getCodigoGiro() {
		return codigoGiro;
	}

	public void setCodigoGiro(Integer codigoGiro) {
		this.codigoGiro = codigoGiro;
	}

	public Integer getCodigoActividad() {
		return codigoActividad;
	}

	public void setCodigoActividad(Integer codigoActividad) {
		this.codigoActividad = codigoActividad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoActividad == null) ? 0 : codigoActividad.hashCode());
		result = prime * result + ((codigoGiro == null) ? 0 : codigoGiro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GiroPK other = (GiroPK) obj;
		if (codigoActividad == null) {
			if (other.codigoActividad != null)
				return false;
		} else if (!codigoActividad.equals(other.codigoActividad))
			return false;
		if (codigoGiro == null) {
			if (other.codigoGiro != null)
				return false;
		} else if (!codigoGiro.equals(other.codigoGiro))
			return false;
		return true;
	}
	
	
	

}
