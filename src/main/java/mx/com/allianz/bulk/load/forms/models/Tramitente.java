package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PTL_TRAMITANTE_TR")
public class Tramitente implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1584411623476792363L;

	@Id 
	@GeneratedValue
	@Column(name="ID_TRAMITANTE")
	private Long id;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="PATERNO")
	private String apellidoPaterno;
	
	@Column(name="MATERNO")
	private String apellidoMaterno;
	
	@Column(name="LADA")
	private int lada;
	
	@Column(name="TELEFONO")
	private String telefono;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="LADA_CEL")
	private int ladaCel;
	
	@Column(name="CELULAR")
	private String celular;
	
	public Tramitente() {
	}

	public Tramitente(String nombre, String apellidoPaterno, String apellidoMaterno, int lada, String telefono,
			String email, int ladaCel, String celular) {
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.lada = lada;
		this.telefono = telefono;
		this.email = email;
		this.ladaCel = ladaCel;
		this.celular = celular;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public int getLada() {
		return lada;
	}

	public void setLada(int lada) {
		this.lada = lada;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getLadaCel() {
		return ladaCel;
	}

	public void setLadaCel(int ladaCel) {
		this.ladaCel = ladaCel;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	
	
}