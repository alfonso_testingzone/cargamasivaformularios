package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Clasificador;

@Transactional
public interface ClasificadorDao extends CrudRepository<Clasificador, Integer> {
} 
