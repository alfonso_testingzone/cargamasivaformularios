package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**Modulo VO de CAT_HOSPITAL_TR
 * En esta clase se encuentran los valores del objeto como
 * atributos , metodos y constructores
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-2 
 * 
 * 
 */

//Librerías
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web.
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_CAT_HOSPITAL_TR")
public class Hospital {
	
	//Atributos de la clase.
	@Id
	@Column(name="ID_HOSPITAL")
	private int idHospital;
	
	@Column(name="COD_GRUPO")
	private int codigoGrupo;
	
	@Column(name="COD_SUBGRUPO")
	private int codigoSubGrupo;
	
	@Column(name="DESCRIPCION_HOSPITAL")
	private String descripcionHospital;

	@Column(name="COD_ESTADO")
	private int codigoEstado;
	
	//Constructor default.
	public Hospital() {

	}
	
	//Constructor sobrecargado.
	public Hospital(int idHospital, int codigoGrupo, int codigoSubGrupo, String descripcionHospital, int codigoEstado) {
		this.idHospital = idHospital;
		this.codigoGrupo = codigoGrupo;
		this.codigoSubGrupo = codigoSubGrupo;
		this.descripcionHospital = descripcionHospital;
		this.codigoEstado = codigoEstado;
	}
	
	//Constructor por cadena concatenada.
		public Hospital(String row) {
			String[] params = row.split("\\t");
			this.idHospital = Integer.parseInt(params[0]);
			this.codigoGrupo = Integer.parseInt(params[1]);
			this.codigoSubGrupo = Integer.parseInt(params[2]);
			this.descripcionHospital = params[3];
			this.codigoEstado = Integer.parseInt(params[4]);
		}
	
	//Getters & Setters.
	public int getIdHospital() {
		return idHospital;
	}
	public void setIdHospital(int idHospital) {
		this.idHospital = idHospital;
	}
	public int getCodigoGrupo() {
		return codigoGrupo;
	}
	public void setCodigoGrupo(int codigoGrupo) {
		this.codigoGrupo = codigoGrupo;
	}
	public int getCodigoSubGrupo() {
		return codigoSubGrupo;
	}
	public void setCodigoSubGrupo(int codigoSubGrupo) {
		this.codigoSubGrupo = codigoSubGrupo;
	}
	public String getDescripcionHospital() {
		return descripcionHospital;
	}
	public void setDescripcionHospital(String descripcionHospital) {
		this.descripcionHospital = descripcionHospital;
	}
	public int getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	@Override
	public String toString() {
		return "Hospital [idHospital=" + idHospital + ", codigoGrupo=" + codigoGrupo + ", codigoSubGrupo="
				+ codigoSubGrupo + ", descripcionHospital=" + descripcionHospital + ", codigoEstado=" + codigoEstado
				+ "]";
	}

}