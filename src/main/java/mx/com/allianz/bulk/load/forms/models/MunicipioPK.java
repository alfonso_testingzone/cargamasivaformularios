package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;


public class MunicipioPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7961974616143888547L;

	protected Integer codigoPais;
	protected Integer codigoEstado;
	protected Integer codigoCiudad;
	protected Integer codigoMunicipio;

	
	public MunicipioPK() {
	}

	public MunicipioPK(Integer codigoPais, Integer codigoEstado, Integer codigoCiudad, Integer codigoMunicipio) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
		this.codigoMunicipio = codigoMunicipio;
	}

	public Integer getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(Integer codigoPais) {
		this.codigoPais = codigoPais;
	}

	public Integer getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public Integer getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(Integer codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}
	
	public Integer getCodigoMunicipio() {
		return codigoMunicipio;
	}
	
	public void setCodigoMunicipio(Integer codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	
	@Override
	public int hashCode() {
		return codigoPais * 1_000_000 + codigoEstado * 10_000 + codigoCiudad + 100 + codigoMunicipio ;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
	        return false;
	    }
	    
	    final MunicipioPK other = (MunicipioPK) obj;
	   
	    if (this.codigoPais == null || other.codigoPais == null ||
	    		this.codigoEstado == null || other.codigoEstado == null ||
	    		this.codigoCiudad == null || other.codigoCiudad == null ||
	    		this.codigoMunicipio == null || other.codigoMunicipio == null){
	    	return false;
	    }
	    
	    if (this.codigoPais != other.codigoPais ||
	    		this.codigoEstado != other.codigoEstado ||
	    		this.codigoCiudad != other.codigoCiudad ||
	    		this.codigoMunicipio != other.codigoMunicipio) {
    		return false;
    	}
	    
	    return true;
	}

	
	
}
