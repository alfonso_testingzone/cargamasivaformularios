package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Producto;

@Transactional
public interface ProductoDao extends CrudRepository<Producto, String> {
} 

