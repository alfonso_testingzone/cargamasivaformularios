package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web.
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_PROVEEDOR_TR")
public class Proveedor  {
	
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PROVEEDOR")
	private int idProveedor;
	@Column(name="CODIGO")
	private String codigo;
	@Column(name="TIPO_PERSONA")
	private String tipoPersona;
	@Column(name="RFC")
	private String rfc;
	@Column(name="CURP")
	private String curp;
	@Column(name="RAZON_SOCIAL")
	private String razonSocial;
	@Column(name="NOMBRE_COMERCIAL")
	private String nombreComercial;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="APPATERNO")
	private String apellidoPaterno;
	@Column(name="APMATERNO")
	private String apellidoMaterno;
	@Column(name="FECHA_NACIMIENTO")
	private String fechaNacimiento;
	@Column(name="CONTRASENA")
	private String contrasenia;
	@Column(name="LADA")
	private String lada;
	@Column(name="TELEFONO")
	private String telefono;
	@Column(name="FAX")
	private String fax;
	@Column(name="CELULAR")
	private String telefonoCelular;
	@Column(name="EMAIL")
	private String email;
	@Column(name="PAGINA_WEB")
	private String paginaWeb;
	@Column(name="CALLE")
	private String calle;
	@Column(name="NO_EXTERIOR")
	private String numeroExterior;
	@Column(name="NO_INTERIOR")
	private String numeroInterior;
	@Column(name="COLONIA")
	private String colonia;
	@Column(name="CODIGO_POSTAL")
	private String codigoPostal;
	@Column(name="DELEGACION_MUNICIPIO")
	private String delegacionMunicipio;
	@Column(name="ESTADO")
	private String estado;
	@Column(name="CIUDAD")
	private String ciudad;
	@Column(name="PAIS")
	private String pais;
	@Column(name="IS_TRANSFERENCIA")
	private boolean isTranferencia;
	@Column(name="IS_HOSPITAL")
	private boolean isHospital;
	@Column(name="BANCO")
	private String banco;
	@Column(name="NO_CUENTA")
	private String numeroCuenta;
	@Column(name="CLABE")
	private String clabe;
	@Column(name="IS_CHEQUE")
	private boolean isCheque;
	@Column(name="COD_GPO_PAGO")
	private String codigoGrupoPago;
	@Column(name="COD_SUBGPO_PAGO")
	private String codigoSubGrupoPago;
	
	
	public Proveedor() {
		
	}


	public Proveedor(int idProveedor, String codigo, String tipoPersona, String rfc, String curp,
			String razonSocial, String nombreComercial, String nombre, String apellidoPaterno, String apellidoMaterno,
			String fechaNacimiento, String contrasenia, String lada, String telefono, String fax,
			String telefonoCelular, String email, String paginaWeb, String calle, String numeroExterior,
			String numeroInterior, String colonia, String codigoPostal, String delegacionMunicipio, String estado,
			String ciudad, String pais, boolean isTranferencia, boolean isHospital, String banco, String numeroCuenta,
			String clabe, boolean isCheque, String codigoGrupoPago, String codigoSubGrupoPago) {
		this.idProveedor = idProveedor;
		this.codigo = codigo;
		this.tipoPersona = tipoPersona;
		this.rfc = rfc;
		this.curp = curp;
		this.razonSocial = razonSocial;
		this.nombreComercial = nombreComercial;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.fechaNacimiento = fechaNacimiento;
		this.contrasenia = contrasenia;
		this.lada = lada;
		this.telefono = telefono;
		this.fax = fax;
		this.telefonoCelular = telefonoCelular;
		this.email = email;
		this.paginaWeb = paginaWeb;
		this.calle = calle;
		this.numeroExterior = numeroExterior;
		this.numeroInterior = numeroInterior;
		this.colonia = colonia;
		this.codigoPostal = codigoPostal;
		this.delegacionMunicipio = delegacionMunicipio;
		this.estado = estado;
		this.ciudad = ciudad;
		this.pais = pais;
		this.isTranferencia = isTranferencia;
		this.isHospital = isHospital;
		this.banco = banco;
		this.numeroCuenta = numeroCuenta;
		this.clabe = clabe;
		this.isCheque = isCheque;
		this.codigoGrupoPago = codigoGrupoPago;
		this.codigoSubGrupoPago = codigoSubGrupoPago;
	}

	
	public Proveedor(String row){
		
		String[] params = row.split("\\t");
		this.idProveedor = Integer.parseInt(params[0]);
		this.codigo = params[1];
		this.tipoPersona = params[2];
		this.rfc = params[3];
		this.curp = params[4];
		this.razonSocial = params[5];
		this.nombreComercial = params[6];
		this.nombre = params[7];
		this.apellidoPaterno = params[8];
		this.apellidoMaterno = params[9];
		this.fechaNacimiento = params[10];
		this.contrasenia = params[11];
		this.lada = params[12];
		this.telefono = params[13];
		this.fax = params[14];
		this.telefonoCelular = params[15];
		this.email = params[16];
		this.paginaWeb = params[17];
		this.calle = params[18];
		this.numeroExterior = params[19];
		this.numeroInterior = params[20];
		this.colonia = params[21];
		this.codigoPostal = params[22];
		this.delegacionMunicipio = params[23];
		this.estado = params[24];
		this.ciudad = params[25];
		this.pais = params[26];
		this.isTranferencia = new Boolean (params[27]);
		this.isHospital = new Boolean (params[28]);
		this.banco = params[29];
		this.numeroCuenta = params[30];
		this.clabe = params[31];
		this.isCheque = new Boolean (params[32]);
		this.codigoGrupoPago = params[33];
		this.codigoSubGrupoPago = params[34];
		
	}

	public int getIdProveedor() {
		return idProveedor;
	}


	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getTipoPersona() {
		return tipoPersona;
	}


	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}


	public String getRfc() {
		return rfc;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getCurp() {
		return curp;
	}


	public void setCurp(String curp) {
		this.curp = curp;
	}


	public String getRazonSocial() {
		return razonSocial;
	}


	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}


	public String getNombreComercial() {
		return nombreComercial;
	}


	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}


	public String getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getContrasenia() {
		return contrasenia;
	}


	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}


	public String getLada() {
		return lada;
	}


	public void setLada(String lada) {
		this.lada = lada;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getTelefonoCelular() {
		return telefonoCelular;
	}


	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPaginaWeb() {
		return paginaWeb;
	}


	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}


	public String getCalle() {
		return calle;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public String getNumeroExterior() {
		return numeroExterior;
	}


	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}


	public String getNumeroInterior() {
		return numeroInterior;
	}


	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}


	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public boolean isTranferencia() {
		return isTranferencia;
	}


	public void setTranferencia(boolean isTranferencia) {
		this.isTranferencia = isTranferencia;
	}


	public boolean isHospital() {
		return isHospital;
	}


	public void setHospital(boolean isHospital) {
		this.isHospital = isHospital;
	}


	public String getBanco() {
		return banco;
	}


	public void setBanco(String banco) {
		this.banco = banco;
	}


	public String getNumeroCuenta() {
		return numeroCuenta;
	}


	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}


	public String getClabe() {
		return clabe;
	}


	public void setClabe(String clabe) {
		this.clabe = clabe;
	}


	public boolean isCheque() {
		return isCheque;
	}


	public void setCheque(boolean isCheque) {
		this.isCheque = isCheque;
	}


	public String getCodigoGrupoPago() {
		return codigoGrupoPago;
	}


	public void setCodigoGrupoPago(String codigoGrupoPago) {
		this.codigoGrupoPago = codigoGrupoPago;
	}


	public String getCodigoSubGrupoPago() {
		return codigoSubGrupoPago;
	}


	public void setCodigoSubGrupoPago(String codigoSubGrupoPago) {
		this.codigoSubGrupoPago = codigoSubGrupoPago;
	}


	@Override
	public String toString() {
		return "Proveedor [idProveedor=" + idProveedor + ", codigo=" + codigo + ", tipoPersona=" + tipoPersona
				+ ", rfc=" + rfc + ", curp=" + curp + ", razonSocial=" + razonSocial + ", nombreComercial="
				+ nombreComercial + ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno="
				+ apellidoMaterno + ", fechaNacimiento=" + fechaNacimiento + ", contrasenia=" + contrasenia + ", lada="
				+ lada + ", telefono=" + telefono + ", fax=" + fax + ", telefonoCelular=" + telefonoCelular + ", email="
				+ email + ", paginaWeb=" + paginaWeb + ", calle=" + calle + ", numeroExterior=" + numeroExterior
				+ ", numeroInterior=" + numeroInterior + ", colonia=" + colonia + ", codigoPostal=" + codigoPostal
				+ ", delegacionMunicipio=" + delegacionMunicipio + ", estado=" + estado + ", ciudad=" + ciudad
				+ ", pais=" + pais + ", isTranferencia=" + isTranferencia + ", isHospital=" + isHospital + ", banco="
				+ banco + ", numeroCuenta=" + numeroCuenta + ", clabe=" + clabe + ", isCheque=" + isCheque
				+ ", codigoGrupoPago=" + codigoGrupoPago + ", codigoSubGrupoPago=" + codigoSubGrupoPago + "]";
	}
	
	
	
}
