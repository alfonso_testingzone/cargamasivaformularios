package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="PTL_CAT_ACTIVIDAD_SAT")
public class Actividad {
	
	@Id
	@Column(name="CODIGO_ACTIVIDAD")
	private int codigoActividad;
	
	@Column(name="DESCRIPCION_ACTIVIDAD")
	private String descripcionActividad;

	public Actividad() {
	}

	
	public Actividad(String row) {
		String[] params = row.split("\\t");
		this.codigoActividad = Integer.parseInt(params[0]);
		this.descripcionActividad = params[1];
	}
	
	
	public Actividad(int codigoActividad, String descripcionActividad) {
		super();
		this.codigoActividad = codigoActividad;
		this.descripcionActividad = descripcionActividad;
	}

	public int getCodigoActividad() {
		return codigoActividad;
	}

	public void setCodigoActividad(int codigoActividad) {
		this.codigoActividad = codigoActividad;
	}

	public String getDescripcionActividad() {
		return descripcionActividad;
	}

	public void setDescripcionActividad(String descripcionActividad) {
		this.descripcionActividad = descripcionActividad;
	}

	@Override
	public String toString() {
		return "Actividad [codigoActividad=" + codigoActividad + ", descripcionActividad=" + descripcionActividad
				+ "]";
	}

	
	

	
}
