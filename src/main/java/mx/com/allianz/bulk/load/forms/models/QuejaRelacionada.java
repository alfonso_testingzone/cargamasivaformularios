package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="QJA_RELACIONADA")
public class QuejaRelacionada implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CVE_REL")
	private String claveRelacion;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="ORDENA")
	private String ordena;
	
	@Column(name="USTEDTIENE")
	private String ustedTiene;
	
	@Column(name="ESUSTED")
	private String esUsted;
	
	@Column(name="CODPROD")
	private String codigoProblema;
	
	
	public QuejaRelacionada() {}
	
	public QuejaRelacionada(String row) {
		super();
		String[] params = row.split("\\t");
		this.claveRelacion = params[0];;
		this.descripcion = params[1];
		this.ordena = params[2];
		this.esUsted = params[3];
		this.ustedTiene = params[4];
		this.codigoProblema = params[5];
	}
	
	public QuejaRelacionada(String claveRelacion, String descripcion, String ordena, String ustedTiene, String esUsted,
			String codigoProblema) {
		super();
		this.claveRelacion = claveRelacion;
		this.descripcion = descripcion;
		this.ordena = ordena;
		this.ustedTiene = ustedTiene;
		this.esUsted = esUsted;
		this.codigoProblema = codigoProblema;
	}

	public String getClaveRelacion() {
		return claveRelacion;
	}
	public void setClaveRelacion(String claveRelacion) {
		this.claveRelacion = claveRelacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoProblema() {
		return codigoProblema;
	}
	public void setCodigoProblema(String codigoProblema) {
		this.codigoProblema = codigoProblema;
	}
	public String getOrdena() {
		return ordena;
	}
	public void setOrdena(String ordena) {
		this.ordena = ordena;
	}
	public String getUstedTiene() {
		return ustedTiene;
	}
	public void setUstedTiene(String ustedTiene) {
		this.ustedTiene = ustedTiene;
	}
	public String getEsUsted() {
		return esUsted;
	}
	public void setEsUsted(String esUsted) {
		this.esUsted = esUsted;
	}

	@Override
	public String toString() {
		return new StringBuilder( "QuejaRelacionada [claveRelacion=" ).append( claveRelacion )
				.append( ", descripcion=" ).append( descripcion )
				.append( ", ordena=").append( ordena )
				.append( ", ustedTiene=" ).append( ustedTiene )
				.append( ", esUsted=" ).append( esUsted )
				.append( ", codigoProblema=" ).append( codigoProblema).append( "]").toString();
	}
}
