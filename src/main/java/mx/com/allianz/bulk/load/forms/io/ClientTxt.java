package mx.com.allianz.bulk.load.forms.io;

public interface ClientTxt {
	
	void cargarPaises(String fileName);
	void cargarEstados(String fileName);
	void cargarCiudades(String fileName);
	void cargarMunicipios(String fileName);
	void cargarSucursales(String fileName);
	void cargarMonedas(String fileName);
	void cargarClasificador(String fileName);
	void cargarHospitales(String fileName);
	void cargarDocumentos(String fileName);
	void cargarGruposPago(String fileName);
	void cargarProductos(String fileName);
	void cargarQuejasRefiere(String fileName);
	void cargarQuejasRelacionada(String fileName);

	
	void cargarTodo();
}
