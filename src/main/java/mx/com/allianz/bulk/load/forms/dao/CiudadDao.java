package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Ciudad;
import mx.com.allianz.bulk.load.forms.models.CiudadPK;

@Transactional
public interface CiudadDao extends CrudRepository<Ciudad, CiudadPK> {
} 
