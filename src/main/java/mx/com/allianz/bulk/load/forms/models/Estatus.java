package mx.com.allianz.bulk.load.forms.models;

import static mx.com.allianz.bulk.load.forms.io.Constants.convertirCadenaFecha;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;







/*@XmlRootElement sirve para instruir la clase ra\u00edz 
 *  que se puede serializar y devolver  
 *  los servicios web
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_ESTATUS_TRAM_TR")

public class Estatus {
	
	@Id
	@Column(name="ID_TRAMITE")
	private int idTramite;
	@Column(name="ID_OT")
	private int idOt; 
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;
	@Column(name="ESTATUS")
	private String estatus;  
	@Column(name="INFO_TRAMITE")
	private String informacionTramite; 
	@Column(name="PROV_ID_PROVEEDOR")
	private int proveIdProvedor;
	
	
	public Estatus() {

	}


	public Estatus(int idTramite, int idOt, Date fechaRegistro, String estatus, String informacionTramite,
			int proveIdProvedor) {
		
		this.idTramite = idTramite;
		this.idOt = idOt;
		this.fechaRegistro = fechaRegistro;
		this.estatus = estatus;
		this.informacionTramite = informacionTramite;
		this.proveIdProvedor = proveIdProvedor;
		
	}
	
	public Estatus(String row){
		
		String[] params = row.split("\\t");

		this.idTramite = Integer.parseInt(params[0]);
		this.idOt = Integer.parseInt(params[1]);
		this.fechaRegistro = convertirCadenaFecha(params[2]);
		this.estatus = params[3];
		this.informacionTramite = params[4];
		this.proveIdProvedor = Integer.parseInt(params[5]);
		
	}


	public int getIdTramite() {
		return idTramite;
	}


	public void setIdTramite(int idTramite) {
		this.idTramite = idTramite;
	}


	public int getIdOt() {
		return idOt;
	}


	public void setIdOt(int idOt) {
		this.idOt = idOt;
	}


	public Date getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getInformacionTramite() {
		return informacionTramite;
	}


	public void setInformacionTramite(String informacionTramite) {
		this.informacionTramite = informacionTramite;
	}


	public int getProveIdProvedor() {
		return proveIdProvedor;
	}


	public void setProveIdProvedor(int proveIdProvedor) {
		this.proveIdProvedor = proveIdProvedor;
	}


	@Override
	public String toString() {
		return "Estatus [idTramite=" + idTramite + ", idOt=" + idOt + ", fechaRegistro=" + fechaRegistro
				+ ", estatus=" + estatus + ", informacionTramite=" + informacionTramite + ", proveIdProvedor="
				+ proveIdProvedor + "]";
	} 
	
	
	
}