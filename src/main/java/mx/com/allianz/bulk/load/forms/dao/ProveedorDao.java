package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Proveedor;

@Transactional
public interface ProveedorDao extends CrudRepository<Proveedor, Integer> {

}
