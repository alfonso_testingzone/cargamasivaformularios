package mx.com.allianz.bulk.load.forms.models;



import static mx.com.allianz.bulk.load.forms.io.Constants.convertirCadenaFecha;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;



/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web.
 */
@XmlRootElement
@Entity 
@Table(name = "QJA_QUEJA")
public class Queja{
	
	@Id
	@Column(name="CVE_QJA")
	private String clave;
	@Column(name="TIPO")
	private String tipo;
	@Column(name="ANO")
	private int anio;
	@Column(name="FOLIO")
	private int folio;
	@Column(name="ESUSTED")
	private String esUsted;
	@Column(name="NUMPOL")
	private String numeroPoliza;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="APELLIDOS")
	private String apellidos;
	@Column(name="RFC")
	private String rfc;
	@Column(name="TELPART")
	private String telefonoParticular;
	@Column(name="TELCEL")
	private String TelefonoCelular;
	@Column(name="MAIL")
	private String email;
	@Column(name="ESTADO")
	private String estado;
	@Column(name="RELACIONADA")
	private String quejaRelacionada;
	@Column(name="REFIERE")
	private String quejaRefiere;
	@Column(name="OBSERVACIONES")
	private String observaciones;
	@Column(name="ARCHIVO")
	private String archivo;
	@Column(name="FECREG")
	private Date fechaRegistro;
	@Column(name="SISTORG")
	private String sistemaOrigen;
	@Column(name="CVEAGTE")
	private String claveAgente;
	@Column(name="NEGOCIO")
	private String negocio;
	@Column(name="EMISOR")
	private String emisor;
	@Column(name="CODPROB")
	private String claveQuejaRelacionada;
	@Column(name="CODSUBPRO")
	private String claveQuejaRefiere;
	@Column(name="RAZON_SOCIAL")
	private String razonSocial;
	
	
	public Queja() {
		
	}


	public Queja(String clave, String tipo, int anio, int folio, String esUsted, String numeroPoliza,
			String nombre, String apellidos, String rfc, String telefonoParticular, String telefonoCelular,
			String email, String estado, String quejaRelacionada, String quejaRefiere, String observaciones,
			String archivo, Date fechaRegistro, String sistemaOrigen, String claveAgente, String negocio,
			String emisor, String claveQuejaRelacionada, String claveQuejaRefiere, String razonSocial) {
		this.clave = clave;
		this.tipo = tipo;
		this.anio = anio;
		this.folio = folio;
		this.esUsted = esUsted;
		this.numeroPoliza = numeroPoliza;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.rfc = rfc;
		this.telefonoParticular = telefonoParticular;
		this.TelefonoCelular = telefonoCelular;
		this.email = email;
		this.estado = estado;
		this.quejaRelacionada = quejaRelacionada;
		this.quejaRefiere = quejaRefiere;
		this.observaciones = observaciones;
		this.archivo = archivo;
		this.fechaRegistro = fechaRegistro;
		this.sistemaOrigen = sistemaOrigen;
		this.claveAgente = claveAgente;
		this.negocio = negocio;
		this.emisor = emisor;
		this.claveQuejaRelacionada = claveQuejaRelacionada;
		this.claveQuejaRefiere = claveQuejaRefiere;
		this.razonSocial = razonSocial;
	}
	
	public Queja(String row){
		
		String[] params = row.split("\\t");
		this.clave = params[0];
		this.tipo = params[1];
		this.anio = Integer.parseInt(params[2]);
		this.folio = Integer.parseInt(params[3]);
		this.esUsted = params[4];
		this.numeroPoliza = params[5];
		this.nombre = params[6];
		this.apellidos = params[7];
		this.rfc = params[8];
		this.telefonoParticular = params[9];
		this.TelefonoCelular = params[10];
		this.email = params[11];
		this.estado = params[12];
		this.quejaRelacionada = params[13];
		this.quejaRefiere = params[14];
		this.observaciones = params[15];
		this.archivo = params[16];
		this.fechaRegistro = convertirCadenaFecha(params[17]);
		this.sistemaOrigen = params[18];
		this.claveAgente = params[19];
		this.negocio = params[20];
		this.emisor = params[21];
		this.claveQuejaRelacionada = params[22];
		this.claveQuejaRefiere = params[23];
		this.razonSocial = params[24];
		
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public int getAnio() {
		return anio;
	}


	public void setAnio(int anio) {
		this.anio = anio;
	}


	public int getFolio() {
		return folio;
	}


	public void setFolio(int folio) {
		this.folio = folio;
	}


	public String getEsUsted() {
		return esUsted;
	}


	public void setEsUsted(String esUsted) {
		this.esUsted = esUsted;
	}


	public String getNumeroPoliza() {
		return numeroPoliza;
	}


	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getRfc() {
		return rfc;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getTelefonoParticular() {
		return telefonoParticular;
	}


	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}


	public String getTelefonoCelular() {
		return TelefonoCelular;
	}


	public void setTelefonoCelular(String telefonoCelular) {
		TelefonoCelular = telefonoCelular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getQuejaRelacionada() {
		return quejaRelacionada;
	}


	public void setQuejaRelacionada(String quejaRelacionada) {
		this.quejaRelacionada = quejaRelacionada;
	}


	public String getQuejaRefiere() {
		return quejaRefiere;
	}


	public void setQuejaRefiere(String quejaRefiere) {
		this.quejaRefiere = quejaRefiere;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public String getArchivo() {
		return archivo;
	}


	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}


	public Date getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public String getSistemaOrigen() {
		return sistemaOrigen;
	}


	public void setSistemaOrigen(String sistemaOrigen) {
		this.sistemaOrigen = sistemaOrigen;
	}


	public String getClaveAgente() {
		return claveAgente;
	}


	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}


	public String getNegocio() {
		return negocio;
	}


	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}


	public String getEmisor() {
		return emisor;
	}


	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}


	public String getClaveQuejaRelacionada() {
		return claveQuejaRelacionada;
	}


	public void setClaveQuejaRelacionada(String claveQuejaRelacionada) {
		this.claveQuejaRelacionada = claveQuejaRelacionada;
	}


	public String getClaveQuejaRefiere() {
		return claveQuejaRefiere;
	}


	public void setClaveQuejaRefiere(String claveQuejaRefiere) {
		this.claveQuejaRefiere = claveQuejaRefiere;
	}


	public String getRazonSocial() {
		return razonSocial;
	}


	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}


	@Override
	public String toString() {
		return "Queja [clave=" + clave + ", tipo=" + tipo + ", anio=" + anio + ", folio=" + folio + ", esUsted="
				+ esUsted + ", numeroPoliza=" + numeroPoliza + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", rfc=" + rfc + ", telefonoParticular=" + telefonoParticular + ", TelefonoCelular=" + TelefonoCelular
				+ ", email=" + email + ", estado=" + estado + ", quejaRelacionada=" + quejaRelacionada
				+ ", quejaRefiere=" + quejaRefiere + ", observaciones=" + observaciones + ", archivo=" + archivo
				+ ", fechaRegistro=" + fechaRegistro + ", sistemaOrigen=" + sistemaOrigen + ", claveAgente="
				+ claveAgente + ", negocio=" + negocio + ", emisor=" + emisor + ", claveQuejaRelacionada="
				+ claveQuejaRelacionada + ", claveQuejaRefiere=" + claveQuejaRefiere + ", razonSocial=" + razonSocial
				+ "]";
	}


	
	
	
}