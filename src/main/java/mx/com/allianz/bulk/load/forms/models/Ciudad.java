package mx.com.allianz.bulk.load.forms.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PTL_CAT_CIUDAD_TR")
@IdClass(CiudadPK.class)
public class Ciudad {
	
	@Id
	@Column(name="COD_PAIS")
	private int codigoPais;
	
	@Id
	@Column(name="COD_ESTADO")
	private int codigoEstado;
	
	@Id
	@Column(name="COD_CIUDAD")
	private int codigoCiudad;

	@Column(name="DESCRIPCION")
	private String descripcion;
	
	public Ciudad() {
	}

	public Ciudad(int codigoPais, int codigoEstado, int codigoCiudad, String descripcion) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
		this.descripcion = descripcion;

	}
	
	public Ciudad(String row){
		String[] params = row.split("\\t");
		this.codigoPais = Integer.parseInt(params[0]);
		this.codigoEstado = Integer.parseInt(params[1]);
		this.codigoCiudad = Integer.parseInt(params[2]);
		this.descripcion = params[3];
	}

	/**
	 * @return the codigoPais
	 */
	public int getCodigoPais() {
		return codigoPais;
	}

	/**
	 * @param codigoPais the codigoPais to set
	 */
	public void setCodigoPais(int codigoPais) {
		this.codigoPais = codigoPais;
	}

	/**
	 * @return the codigoEstado
	 */
	public int getCodigoEstado() {
		return codigoEstado;
	}

	/**
	 * @param codigoEstado the codigoEstado to set
	 */
	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	/**
	 * @return the codigoCiudad
	 */
	public int getCodigoCiudad() {
		return codigoCiudad;
	}

	/**
	 * @param codigoCiudad the codigoCiudad to set
	 */
	public void setCodigoCiudad(int codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Ciudad [codigoPais=" + codigoPais + ", codigoEstado=" + codigoEstado + ", codigoCiudad=" + codigoCiudad
				+ ", descripcion=" + descripcion + "]";
	}

	
	
 }
