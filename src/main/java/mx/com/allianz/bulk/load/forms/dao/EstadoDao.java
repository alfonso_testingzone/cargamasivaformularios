package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Estado;
import mx.com.allianz.bulk.load.forms.models.EstadoPK;

@Transactional
public interface EstadoDao extends CrudRepository<Estado, EstadoPK> {
} 
