package mx.com.allianz.bulk.load.forms.models;

import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;



/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web.
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_SUBGRUPOS_PAGO_TR")
public class Subgrupo {
	
	@Id
	@Column(name="ID_SUBGRUPO")
	private int idSubGrupo;
	@Column(name="CODSUBGRUPO")
	private String codigoSubGrupo;
	@Column(name="CODGRUPOPAG")
	private String codigoGrupoPago;
	@Column(name="DESCSUBGRUPO")
	private String descripcionSubGrupo;
	@Column(name="CODPAIS")
	private String codigoPais;
	@Column(name="CODESTADO")
	private String codigoEstado;
	@Column(name="CODCIUDAD")
	private String codigoCiudad;
	@Column(name="CODMUNICIPIO")
	private String codigoMunicipio;
	@Column(name="DIREC")
	private String direccion;
	@Column(name="NOMCONTACTO")
	private String nombreContacto;
	@Column(name="TELEF1")
	private String telefono;
	@Column(name="ZIP")
	private String zip;
	@Column(name="COLONIA")
	private String colonia;
	@Column(name="STSCODSUBGRUPO")
	private String stsCodigoSubGrupo;
	@Column(name="TIPOID")
	private String tipoId;
	@Column(name="NUMID")
	private String numeroId;
	@Column(name="DVID")
	private String dvId;
	
	//Constructor default.
	public Subgrupo() {
		
	}

	//Constructor sobrecargado.
	public Subgrupo(int idSubGrupo, String codigoSubGrupo, String codigoGrupoPago, String descripcionSubGrupo,
			String codigoPais, String codigoEstado, String codigoCiudad, String codigoMunicipio, String direccion,
			String nombreContacto, String telefono, String zip, String colonia, String stsCodigoSubGrupo, String tipoId,
			String numeroId, String dvId) {
		this.idSubGrupo = idSubGrupo;
		this.codigoSubGrupo = codigoSubGrupo;
		this.codigoGrupoPago = codigoGrupoPago;
		this.descripcionSubGrupo = descripcionSubGrupo;
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
		this.codigoMunicipio = codigoMunicipio;
		this.direccion = direccion;
		this.nombreContacto = nombreContacto;
		this.telefono = telefono;
		this.zip = zip;
		this.colonia = colonia;
		this.stsCodigoSubGrupo = stsCodigoSubGrupo;
		this.tipoId = tipoId;
		this.numeroId = numeroId;
		this.dvId = dvId;
	}
	
	
	public Subgrupo(String row){
		
		String[] params = row.split(Pattern.quote("|"));
		this.idSubGrupo = Integer.parseInt(params[0]);
		this.codigoGrupoPago = params[1];
		this.codigoSubGrupo = params[2];
		this.descripcionSubGrupo = params[3];
		this.codigoPais = params[4];
		this.codigoEstado = params[5];
		this.codigoCiudad = params[6];
		this.codigoMunicipio = params[7];
		this.direccion = params[8];
		this.nombreContacto = params[9];
		this.telefono = params[10];
		this.zip = params[11];
		this.colonia = params[12];
		this.stsCodigoSubGrupo = params[13];
		this.tipoId = params[14];
		this.numeroId = params[15];
		this.dvId = params[16];
		
		
	}
	
	


	public int getIdSubGrupo() {
		return idSubGrupo;
	}


	public void setIdSubGrupo(int idSubGrupo) {
		this.idSubGrupo = idSubGrupo;
	}


	public String getCodigoSubGrupo() {
		return codigoSubGrupo;
	}


	public void setCodigoSubGrupo(String codigoSubGrupo) {
		this.codigoSubGrupo = codigoSubGrupo;
	}


	public String getCodigoGrupoPago() {
		return codigoGrupoPago;
	}


	public void setCodigoGrupoPago(String codigoGrupoPago) {
		this.codigoGrupoPago = codigoGrupoPago;
	}


	public String getDescripcionSubGrupo() {
		return descripcionSubGrupo;
	}


	public void setDescripcionSubGrupo(String descripcionSubGrupo) {
		this.descripcionSubGrupo = descripcionSubGrupo;
	}


	public String getCodigoPais() {
		return codigoPais;
	}


	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}


	public String getCodigoEstado() {
		return codigoEstado;
	}


	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}


	public String getCodigoCiudad() {
		return codigoCiudad;
	}


	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}


	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}


	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getNombreContacto() {
		return nombreContacto;
	}


	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getStsCodigoSubGrupo() {
		return stsCodigoSubGrupo;
	}


	public void setStsCodigoSubGrupo(String stsCodigoSubGrupo) {
		this.stsCodigoSubGrupo = stsCodigoSubGrupo;
	}


	public String getTipoId() {
		return tipoId;
	}


	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}


	public String getNumeroId() {
		return numeroId;
	}


	public void setNumeroId(String numeroId) {
		this.numeroId = numeroId;
	}


	public String getDvId() {
		return dvId;
	}


	public void setDvId(String dvId) {
		this.dvId = dvId;
	}


	@Override
	public String toString() {
		return "Subgrupo [idSubGrupo=" + idSubGrupo + ", codigoSubGrupo=" + codigoSubGrupo + ", codigoGrupoPago="
				+ codigoGrupoPago + ", descripcionSubGrupo=" + descripcionSubGrupo + ", codigoPais=" + codigoPais
				+ ", codigoEstado=" + codigoEstado + ", codigoCiudad=" + codigoCiudad + ", codigoMunicipio="
				+ codigoMunicipio + ", direccion=" + direccion + ", nombreContacto=" + nombreContacto + ", telefono="
				+ telefono + ", zip=" + zip + ", colonia=" + colonia + ", stsCodigoSubGrupo=" + stsCodigoSubGrupo
				+ ", tipoId=" + tipoId + ", numeroId=" + numeroId + ", dvId=" + dvId + "]";
	}
	
	
	
	
	
	
}