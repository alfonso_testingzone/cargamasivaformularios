package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**Modulo VO de documentos
 * En esta clase se encuentran los valores del objeto como
 * atributos , m\u00e9todos y constructores
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-5 
 * 
 * 
 */


import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00edz 
 *  que se puede serializar y devolver  
 *  los servicios web
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_CAT_DOCUMENTOS_TR")
public class Documento {
	
	//Atributos de la clase
	@Id
	@Column(name="ID_DOCUMENTO")
	private String idDocumentos;
	
	@Column(name="TIPO")
	private String tipo;
	
	@Column(name="CODIGO")
	private String codigo;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	//Constructor Default
	public Documento() {

	}
	//Constructor sobrecargado
	public Documento(String idDocumentos, String tipo, String codigo, String descripcion) {
		super();
		this.idDocumentos = idDocumentos;
		this.tipo = tipo;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	//Constructor de fila
		public Documento(String row) {
			super();
			String[] params = row.split("\\t");
			this.idDocumentos = params[0];
			this.descripcion = params[1];
			this.tipo = params[2];
			this.codigo = params[3];
		}
	
	//Getters & Setters
	public String getIdDocumentos() {
		return idDocumentos;
	}
	public void setIdDocumentos(String idDocumentos) {
		this.idDocumentos = idDocumentos;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "Documento [idDocumentos=" + idDocumentos + ", tipo=" + tipo + ", codigo=" + codigo + ", descripcion="
				+ descripcion + "]";
	}
	
	
	
}
