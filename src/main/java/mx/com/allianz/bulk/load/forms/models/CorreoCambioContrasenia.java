package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "CORREO_CAMBIO_CONTRASENIA")
public class CorreoCambioContrasenia implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3156040749051556043L;

	@Id 
	@Column(name="ID_CORREO")
	private String idCorreo;
	
	@Column(name="ID_PROVEEDOR")
	private String idProveedor;
	
	@Column(name="RFC_PROVEEDOR")
	private String rfcProveedor;
	
	@Column(name="VIGENCIA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date vigencia;
	
	@Column(name="ESTATUS")
	private boolean estatus;

	public CorreoCambioContrasenia() {
	}
	
	public CorreoCambioContrasenia(String idCorreo, String idProveedor, String rfcProveedor, Date vigencia,
			boolean estatus) {
		this.idCorreo = idCorreo;
		this.idProveedor = idProveedor;
		this.rfcProveedor = rfcProveedor;
		this.vigencia = vigencia;
		this.estatus = estatus;
	}

	public String getIdCorreo() {
		return idCorreo;
	}

	public void setIdCorreo(String idCorreo) {
		this.idCorreo = idCorreo;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRfcProveedor() {
		return rfcProveedor;
	}

	public void setRfcProveedor(String rfcProveedor) {
		this.rfcProveedor = rfcProveedor;
	}

	public Date getVigencia() {
		return vigencia;
	}

	public void setVigencia(Date vigencia) {
		this.vigencia = vigencia;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

}
