package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.QuejaRelacionada;

@Transactional
public interface QuejaRelacionadaDao extends CrudRepository<QuejaRelacionada, String> {
}
