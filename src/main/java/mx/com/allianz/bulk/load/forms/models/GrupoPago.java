package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00edz 
 *  que se puede serializar y devolver  
 *  los servicios web
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_CAT_GRUPOS_PAGO_TR")
public class GrupoPago {
	
	//Atributos de la clase
	@Id
	@Column(name="CODGRUPOPAG")
	private int codigoGrupoPago;
	
	@Column(name="DESCRIPGRUPO")
	private String descripcionGrupo;

	@Column(name="STSDIRGRUPOPAG")
	private String sistemaDirectorioGrupoPago;
	
	//Constructor Default
	public GrupoPago() {
		
	}
	//Constructor sobrecargado
	public GrupoPago(int codigoGrupoPago, String descripcionGrupo, String sistemaDirectorioGrupoPago) {
		this.codigoGrupoPago = codigoGrupoPago;
		this.descripcionGrupo = descripcionGrupo;
		this.sistemaDirectorioGrupoPago = sistemaDirectorioGrupoPago;
	}
	
	//Constructor de fila
		public GrupoPago(String row) {
			String[] params = row.split("\\t");
			this.codigoGrupoPago = Integer.parseInt(params[0]);
			this.descripcionGrupo = params[1];
			this.sistemaDirectorioGrupoPago = params[2];
		}
	
	//Getters & Setters
	
	public int getCodigoGrupoPago() {
		return codigoGrupoPago;
	}
	public void setCodigoGrupoPago(int codigoGrupoPago) {
		this.codigoGrupoPago = codigoGrupoPago;
	}
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	public String getSistemaDirectorioGrupoPago() {
		return sistemaDirectorioGrupoPago;
	}
	public void setSistemaDirectorioGrupoPago(String sistemaDirectorioGrupoPago) {
		this.sistemaDirectorioGrupoPago = sistemaDirectorioGrupoPago;
	}
	
	
	@Override
	public String toString() {
		return "GrupoPago [codigoGrupoPago=" + codigoGrupoPago + ", descripcionGrupo=" + descripcionGrupo
				+ ", sistemaDirectorioGrupoPago=" + sistemaDirectorioGrupoPago + "]";
	}



}
