package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;


public class CiudadPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7961974616143888547L;

	protected Integer codigoPais;
	protected Integer codigoEstado;
	protected Integer codigoCiudad;
	
	public CiudadPK() {
	}

	public CiudadPK(Integer codigoPais, Integer codigoEstado, Integer codigoCiudad) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
	}

	public Integer getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(Integer codigoPais) {
		this.codigoPais = codigoPais;
	}

	public Integer getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public Integer getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(Integer codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}
	
	@Override
	public int hashCode() {
		return codigoPais * 1_000_000 + codigoEstado * 10_000 + codigoCiudad + 100 ;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
	        return false;
	    }
	    
	    final CiudadPK other = (CiudadPK) obj;
	   
	    if (this.codigoPais == null || other.codigoPais == null ||
	    		this.codigoEstado == null || other.codigoEstado == null ||
	    		this.codigoCiudad == null || other.codigoCiudad == null){
	    	return false;
	    }
	    
	    if (this.codigoPais != other.codigoPais ||
	    		this.codigoEstado != other.codigoEstado ||
	    		this.codigoCiudad != other.codigoCiudad) {
    		return false;
    	}
	    
	    return true;
	}
	
	
}
