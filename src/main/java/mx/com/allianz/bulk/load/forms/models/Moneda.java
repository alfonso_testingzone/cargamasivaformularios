package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/*@XmlRootElement sirve para instruir la clase ra\u00ed 
 *  que se puede serializar y devolver  
 *  los servicios web.
 */
@XmlRootElement
@Entity 
@Table(name = "PTL_CAT_MONEDA")
public class Moneda {
	
	//Atributos de la clase.
	@Id
	@Column(name="ID_MONEDA")
	private int idMoneda;
	
	@Column(name="COD_MONEDA")
	private String codigoMoneda;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	
	//Constructor default.
	public Moneda() {
		
	}
	
	
	//Constructor sobrecargado.
	public Moneda(int idMoneda, String codigoMoneda, String descripcion) {
		this.idMoneda = idMoneda;
		this.codigoMoneda = codigoMoneda;
		this.descripcion = descripcion;
	}
	
	public Moneda(String row) {
		String[] params = row.split("\\t");
		this.idMoneda = Integer.parseInt(params[0]);
		this.codigoMoneda = params[1];
		this.descripcion = params[2];

	}
	
	//Getters & Setters	
	public int getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(int idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getCodigoMoneda() {
		return codigoMoneda;
	}
	public void setCodigoMoneda(String codigoMoneda) {
		this.codigoMoneda = codigoMoneda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	@Override
	public String toString() {
		return "Moneda [idMoneda=" + idMoneda + ", codigoMoneda=" + codigoMoneda + ", descripcion=" + descripcion + "]";
	}
	



}
