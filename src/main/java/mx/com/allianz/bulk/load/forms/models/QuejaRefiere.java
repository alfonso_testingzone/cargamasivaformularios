package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="QJA_REFIERE")
@IdClass(QuejaRefierePK.class)
public class QuejaRefiere {
	
	@Id
	@Column(name="CVE_REF")
	protected String claveReferencia;
	
	@Id
	@Column(name="CVE_REL")
	protected String claveRelacion;
	
	@Column(name="CODSUBPROD")
	private String codigoSubProducto;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="ORDENA")
	private String ordena;
	
	@Column(name="FLUJO_PUC")
	private String flujoPuc;
	
	
	public QuejaRefiere(){}

	public QuejaRefiere(String claveRef, String codigoSubProducto, String descripcion,
			String claveRel) {
		super();
		this.claveReferencia = claveRef;
		this.claveRelacion = claveRel;
		this.codigoSubProducto = codigoSubProducto;
		this.descripcion = descripcion;
	}
	
	public QuejaRefiere(String row) {
		super();
		String[] params = row.split("\\t");
		this.claveReferencia = params[0];
		this.codigoSubProducto = params[1];
		this.descripcion = params[2];
		this.ordena = params[3];
		this.flujoPuc = params[4];
		this.claveRelacion = params[5];
	}

	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}

	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getClaveReferencia() {
		return claveReferencia;
	}

	public void setClaveReferencia(String claveReferencia) {
		this.claveReferencia = claveReferencia;
	}

	public String getClaveRelacion() {
		return claveRelacion;
	}

	public void setClaveRelacion(String claveRelacion) {
		this.claveRelacion = claveRelacion;
	}
	
	public String getOrdena() {
		return ordena;
	}

	public void setOrdena(String ordena) {
		this.ordena = ordena;
	}

	public String getFlujoPuc() {
		return flujoPuc;
	}

	public void setFlujoPuc(String flujoPuc) {
		this.flujoPuc = flujoPuc;
	}

	@Override
	public String toString() {
		return new StringBuilder("QuejaRefiere [claveRelacion=" ).append( claveReferencia )
				.append( ", codigoSubProducto=" ).append( codigoSubProducto)
				.append( ", descripcion=" ).append( descripcion )
				.append( ", ordena=" ).append( ordena )
				.append( ", flujoPuc=" ).append( flujoPuc )
				.append( ", claveRel=").append( claveRelacion ).append( "]").toString();
	}
}
