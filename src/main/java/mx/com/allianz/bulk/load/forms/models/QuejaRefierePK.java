package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;

public class QuejaRefierePK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 10000L;

	protected String claveReferencia;
	
	protected String claveRelacion;
	
	public QuejaRefierePK(){}

	public QuejaRefierePK(String claveReferencia, String claveRelacion) {
		super();
		this.claveReferencia = claveReferencia;
		this.claveRelacion = claveRelacion;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((claveReferencia == null) ? 0 : claveReferencia.hashCode());
		result = prime * result + ((claveRelacion == null) ? 0 : claveRelacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		QuejaRefierePK other = (QuejaRefierePK) obj;
		if (claveReferencia == null) {
			if (other.claveReferencia != null)
				return false;
		} else if (!claveReferencia.equals(other.claveReferencia))
			return false;
		if (claveRelacion == null) {
			if (other.claveRelacion != null)
				return false;
		} else if (!claveRelacion.equals(other.claveRelacion))
			return false;
		return true;
	}

	public String getClaveReferencia() {
		return claveReferencia;
	}

	public void setClaveReferencia(String claveReferencia) {
		this.claveReferencia = claveReferencia;
	}

	public String getClaveRelacion() {
		return claveRelacion;
	}

	public void setClaveRelacion(String claveRelacion) {
		this.claveRelacion = claveRelacion;
	}
	
	
	
}