package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.Municipio;
import mx.com.allianz.bulk.load.forms.models.MunicipioPK;

@Transactional
public interface MunicipioDao extends CrudRepository<Municipio, MunicipioPK> {
} 
