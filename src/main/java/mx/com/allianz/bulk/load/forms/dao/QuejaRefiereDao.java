package mx.com.allianz.bulk.load.forms.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.bulk.load.forms.models.QuejaRefiere;
import mx.com.allianz.bulk.load.forms.models.QuejaRefierePK;

@Transactional
public interface QuejaRefiereDao extends CrudRepository<QuejaRefiere, QuejaRefierePK> {
}
