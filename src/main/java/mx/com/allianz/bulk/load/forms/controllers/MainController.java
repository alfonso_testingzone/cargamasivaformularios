package mx.com.allianz.bulk.load.forms.controllers;

import java.util.Locale;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.allianz.bulk.load.forms.dao.PaisDao;
import mx.com.allianz.bulk.load.forms.io.ClientTxt;

@Controller
public class MainController {
	
	public static final String FILE_COUNTRY = "Pais.txt";
	
	@Autowired
	private ClientTxt clienteTxt;

	@Autowired
	private PaisDao paisdao; 	  
  
	@RequestMapping("/")
	@ResponseBody
	public String createAll() {
		
		System.out.println("Begin Bulk Load");
		clienteTxt.cargarTodo();
		System.out.println("End Bulk Load");

		return " All created :)";
	}
	
	@RequestMapping("/pais/create")
	@ResponseBody
	public String createPais() {
		Locale locale = new Locale("es", "ES");
	    Locale.setDefault(locale);
		clienteTxt.cargarPaises(FILE_COUNTRY);
		
		return " Country created :)";
	}
  
	@RequestMapping("/pais/getAll")
	@ResponseBody
	public String getAll() {
		StringJoiner join = new StringJoiner(", ");
		try {
			paisdao.findAll().forEach(pais -> join.add(pais.toString()));
	    }catch (Exception ex) {
	      return "Error loading coutries: " + ex.toString();
	    }
	    return "Load countries! \n " + join.toString();
	}
	
	@RequestMapping("/pais/removeAll")
	@ResponseBody
	public String removeAll() {
		try {
			paisdao.deleteAll();
	    }catch (Exception ex) {
	      return "Error loading coutries: " + ex.toString();
	    }
	    return "removed countries!";
	}
  
}
