package mx.com.allianz.bulk.load.forms.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PTL_AFECTADO_TR")
public class Afectado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2423757319743586546L;

	@Id 
	@GeneratedValue
	@Column(name="ID_AFECTADO")
	private Long id;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="PATERNO")
	private String apellidoPaterno;
	
	@Column(name="MATERNO")
	private String apellidoMaterno;

	public Afectado() {
	}
	
	public Afectado(String nombre, String apellidoPaterno, String apellidoMaterno) {
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	
}
