package mx.com.allianz.bulk.load.forms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="PTL_CAT_ESTADO_TR")
@IdClass(EstadoPK.class)
public class Estado {
	
	@Id
	@Column(name="COD_PAIS")
	private int codigoPais;
	
	@Id
	@Column(name="COD_ESTADO")
	private int codigoEstado;
	
	@Column(name="DESCRIPCION")
	private String descripcion;

	public Estado() {
		
	}
	
	public Estado(String row) {
		String[] params = row.split("\\t");
		this.codigoPais = Integer.parseInt(params[0]);
		this.codigoEstado = Integer.parseInt(params[1]);
		this.descripcion = params[2];
	}

	public Estado(int codigoPais, int codigoEstado, String descripcion) {
		this.codigoEstado = codigoEstado;
		this.codigoPais = codigoPais;
		this.descripcion = descripcion;
	}

	
	// getter & setters
	public int getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(int codigoPais) {
		this.codigoPais = codigoPais;
	}

	public int getCodigoEstado() {
		return codigoEstado;
	}
	
	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Estado [codigoPais=" + codigoPais + ", codigoEstado=" + codigoEstado + ", description=" + descripcion + "]";
	}


}
